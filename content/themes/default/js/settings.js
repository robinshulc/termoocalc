var set = {
    ajaxMethod: 'POST',

    ChangeRegion: function() {

        var formData = new FormData();
        formData.append('region', $('#selectRegion').val());
        // console.log($('#selectRegion').val());
        $.ajax({
            url: '/get-cities',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log();
                $("#selectCity").html(result);

            }
        });

    },

    ChangeCity: function() {

        var formData = new FormData();
        formData.append('city', $('#selectCity').val());
        // console.log($('#selectRegion').val());
        $.ajax({
            url: '/get-city-params',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log(result);
                $("#cityTbody").html(result);

            }
        });
        formData.append('city', $('#selectCity').val());
        formData.append('building', $('#selectBuilding').val());
        formData.append('room', $('#selectRoom').val());

        $.ajax({
            url: '/get-cities-buildings',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log(result);
                $("#AllTbody").html(result);

            }
        });
    },

    ChangeBuilding: function() {

        var formData = new FormData();
        formData.append('building', $('#selectBuilding').val());
        // console.log($('#selectRegion').val());
        $.ajax({
            url: '/get-building-params',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log(result);
                $("#buildingTbody").html(result);

            }
        });

        formData.append('city', $('#selectCity').val());
        formData.append('building', $('#selectBuilding').val());
        formData.append('room', $('#selectRoom').val());

        $.ajax({
            url: '/get-cities-buildings',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log((result));
                $("#AllTbody").html(result);

            }
        });
    },

    ChangeRoom: function() {

        var formData = new FormData();
        formData.append('room', $('#selectRoom').val());
        // console.log($('#selectRegion').val());
        $.ajax({
            url: '/get-room-params',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log(result);
                $("#roomTbody").html(result);

            }
        });
    },


    ChangeCoefficient: function() {

        var formData = new FormData();
        formData.append('coefficient', $('#selectCoefficient').val());
        // console.log($('#selectMat_1').val());
        $.ajax({
            url: '/get-coefficient',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log(result);
                $("#change_coefficient").val(result);

            }
        });

    },


    ChangeMaterialCat: function(element) {


        var formData = new FormData();
        formData.append('material_id', $(element).val());
        var numb = ($(element).parent().parent().attr('class'));
        var result = numb.split('_');
        numb = result[1];
        // console.log(numb)
        $.ajax({
            url: '/get-mat_cat',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){

                $('[data-tab="'+numb+'"]').html(result);

            }
        });

    },

    ChangeMaterialCatBlocks: function(element) {
        var section = 1;
        var formData = new FormData();
        formData.append('material_id', $(element).val());
        var numb = ($(element).parent().parent().attr('class'));
        var result = numb.split('_');
        numb = result[1];
        var select = $(element).parent();

        if(select.hasClass( "second_select" )){
            section = 2;
        }
        $.ajax({
            url: '/get-mat_cat_blocks',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log(result);
                // console.log(section);
                // console.log(numb);
                if(section == 1){
                    $('[data-tab="'+numb+'"]').find(".table_first").html(result);
                }else{
                    $('[data-tab="'+numb+'"]').find(".table_second").html(result);
                }

            }
        });

    },

    ChangeRoomType: function() {

        var roomType_id = $('#selectRoom').val();
        var a_or_b = $('#AllTbody .roomType_'+roomType_id);
        a_or_b = (a_or_b).parent().next('tr').next('tr').next('tr').next('tr').find('td:nth-child(2)').text();
        var formData = new FormData();
        formData.append('room', $('#selectRoom').val());
        // console.log(a_or_b);
        $.ajax({
            url: '/get-room-type',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                var obj = JSON.parse(result)
                // console.log(obj);
                $("#room_type").val(obj.stp02);
                $("#AllTbodyWall .a_or_b").val(a_or_b);

            }
        });
    },

    ChangeBuildingWallPage: function() {

        var formData = new FormData();

        formData.append('city', $('#selectCity').val());
        formData.append('building', $('#selectBuilding').val());
        formData.append('room', $('#selectRoom').val());

        $.ajax({
            url: '/get-cities-buildings',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log((result));
                $("#AllTbodyWall .td37").val($(result).find('.td37').text());
                $("#AllTbodyWall .td40").val($(result).find('.td40').text());
                $("#AllTbody").html(result);





            }
        });
    },

    ChangeCityWallPage: function() {
        var formData = new FormData();

        formData.append('city', $('#selectCity').val());
        formData.append('building', $('#selectBuilding').val());
        formData.append('room', $('#selectRoom').val());

        $.ajax({
            url: '/get-cities-buildings',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log(result);
                $("#AllTbodyWall .td37").val($(result).find('.td37').text());
                $("#AllTbodyWall .td40").val($(result).find('.td40').text());
                $("#AllTbody").html(result);

                $('#calculate').removeAttr('disabled');
                $('#selectRoom').removeAttr('disabled');

                setTimeout(function () {
                    var roomType_id = $('#selectRoom').val();
                    var a_or_b = $('#AllTbody .roomType_'+roomType_id);
                    a_or_b = (a_or_b).parent().next('tr').next('tr').next('tr').next('tr').find('td:nth-child(2)').text();
                    $("#AllTbodyWall .a_or_b").val(a_or_b);

                }, 1000);
            }
        });
    },

};