
var arr_tmy = {
    1638:-40,
    1504:-39,
    1382:-38,
    1271:-37,
    1170:-36,
    1077:-35,
    992.7:-34,
    915.5:-33,
    844.8:-32,
    780.2:-31,
    721.01:-30,
    666.7:-29,
    616.9:-28,
    571.2:-27,
    520.2:-26,
    490.7:-25,
    455.2:-24,
    422.5:-23,
    392.5:-22,
    364.8:-21,
    339.2:-20,
    315.6:-19,
    293.9:-18,
    273.8:-17,
    255.2:-16,
    238:-15,
    222.1:-14,
    207.4:-13,
    193.7:-12,
    181.1:-11,
    169.3:-10,
    158.4:-9,
    148.3:-8,
    138.9:-7,
    130.2:-6,
    122.1:-5,
    114.5:-4,
    107.5:-3,
    100.9:-2,
    94.8:-1,
    89.1:0,
    83.8:1,
    78.8:2,
    74.2:3,
    69.9:4,
    65.8:5,
    62.01:6,
    58.5:7,
    55.2:8,
    52.1:9,
    49.1:10,
    46.4:11,
    43.9:12,
    41.5:13,
    39.2:14,
    37.1:15,
    35.1:16,
    33.2:17,
    31.5:18,
    29.8:19,
    28.3:20,
    26.8:21,
    25.4:22,
    23.8:23,
    22.5:24,
    21.1:25,
    18.7:26,
    17.9:27,
};

$("#calculate").click(function(){
//
// if((($("#AllTbodyWall .td37").val())*1) == 0){
//
//     $('#calculate').attr('enabled',true);
//     // alert('вы не выбрали город ');
//     // return false;
// }
    $('#calk-tbody').html("")
    $('#calk-tbody').show();
    var tables = $('#dataTbody');
    // console.log(tables);
    var table = tables.find('.table');
    // console.log(table);
    var l = table.length; // total items

    var td_CTM38 = 0;
    var sum_td_CTM36 = 0;
    var sum_td_CTM37 = 0;

    for (var i=0;i<l;i++){
        var j = (i+1);
        var element = table[i];
        var numb = ($(element).parent().parent().attr('class'));
        var result = numb.split('_');
        numb = result[1];



        var thickness = $('#ol').find('.li_'+numb).find('.thickness').val();
        // console.log(thickness);

        var td_name = $(element).find('.name').text();
        var td_CTM02 = $(element).find('.COL6').text();
        var td_CTM03 = $(element).find('.COL7').text();
        var td_CTM04 = $(element).find('.COL8').text();
        if($("#AllTbodyWall .a_or_b").val() == 'a'){

            var td_CTM05 = $(element).find('.COL9').text();
            var td_CTM06 = 'A';

            var td_CTM07 = $(element).find('.COL11').text();
            var td_CTM08 = td_CTM06;
            var td_CTM09 = $(element).find('.COL13').text();
            var td_CTM10 = td_CTM06;

        }else{

            var td_CTM05 = $(element).find('.COL10').text();
            var td_CTM06 = 'B';

            var td_CTM07 = $(element).find('.COL12').text();
            var td_CTM08 = td_CTM06;
            var td_CTM09 = $(element).find('.COL14').text();
            var td_CTM10 = td_CTM06;
        }

        var td_CTM11 = $(element).find('.COL15').text();
        var td_CTM13 = $(element).find('.COL17').text();
        var td_CTM14 = $(element).find('.COL18').text();
        var td_CTM16 = $(element).find('.COL20').text();
        var td_CTM17 = $(element).find('.COL21').text();
        var td_CTM18 = $(element).find('.COL22').text();
        var td_CTM19 = thickness;
        var td_CTM20 = thickness*0.001;
        var td_CTM21 = $(element).find('.COL25').text();;
        var td_CTM22 = $(element).find('.COL26').text();;
        var td_CTM23 = $(element).find('.COL27').text();;
        var td_CTM24 = $(element).find('.COL28').text();;
        var td_CTM25 = '';
        var td_CTM26 = '';
        var td_CTM27 = '';
        var td_CTM28 = '';
        var td_CTM29 = '';

        var td_CTM36 = '';
        var td_CTM37 = '';
        // var td_CTM38 = '';
        var td_CTM39 = '';
        var td_CTM40 = '';
        var td_CTM41 = '';
        if($(element).hasClass('table_first')){
            // j = j-1;
            // console.log(i);
            i++;
            td_name = td_name +' + '+$(table[i]).find('.name').text();

            var data_input = $('#ol').find('.li_'+numb).find('.data-input-block');

            if($(data_input).hasClass('block-input')){
                td_CTM27 = (data_input.find('.block-w').val());
                td_CTM28 = (data_input.find('.block-h').val());
                td_CTM29 = (data_input.find('.block-seam').val());
            }
            if($(data_input).hasClass('frame-input')){
                td_CTM25 = (data_input.find('.frame-between').val())*1;
                td_CTM26 = (data_input.find('.frame-width').val())*1;


                var next_td_CTM02 = $(table[i]).find('.COL6').text();
                var next_td_CTM03 = $(table[i]).find('.COL7').text();
                var next_td_CTM04 = $(table[i]).find('.COL8').text();



                if($("#AllTbodyWall .a_or_b").val() == 'a'){

                    var next_td_CTM05 = $(table[i]).find('.COL9').text();
                    var next_td_CTM06 = 'A';
                    var next_td_CTM07 = $(table[i]).find('.COL11').text();
                    var next_td_CTM08 = next_td_CTM06;
                    var next_td_CTM09 = $(table[i]).find('.COL13').text();
                    var next_td_CTM10 = next_td_CTM06;
                }else{

                    var next_td_CTM05 = $(table[i]).find('.COL10').text();
                    var next_td_CTM06 = 'B';
                    var next_td_CTM07 = $(table[i]).find('.COL12').text();
                    var next_td_CTM08 = next_td_CTM06;
                    var next_td_CTM09 = $(table[i]).find('.COL14').text();
                    var next_td_CTM10 = next_td_CTM06;
                }



                var next_td_CTM11 = $(table[i]).find('.COL15').text();
                var next_td_CTM13 = $(table[i]).find('.COL17').text();

                var new_td_CTM02 = (td_CTM25 + td_CTM26)/((td_CTM25/next_td_CTM02)+(td_CTM26/td_CTM02));
                var new_td_CTM03 = (td_CTM25 + td_CTM26)/((td_CTM25/next_td_CTM03)+(td_CTM26/td_CTM03));
                var new_td_CTM04 = (td_CTM25 + td_CTM26)/((td_CTM25/next_td_CTM04)+(td_CTM26/td_CTM04));
                var new_td_CTM05 = (td_CTM25 + td_CTM26)/((td_CTM25/next_td_CTM05)+(td_CTM26/td_CTM05));
                var new_td_CTM06 = (td_CTM25 + td_CTM26)/((td_CTM25/next_td_CTM06)+(td_CTM26/td_CTM06));
                var new_td_CTM07 = (td_CTM25 + td_CTM26)/((td_CTM25/next_td_CTM07)+(td_CTM26/td_CTM07));
                var new_td_CTM08 = (td_CTM25 + td_CTM26)/((td_CTM25/next_td_CTM08)+(td_CTM26/td_CTM08));
                var new_td_CTM09 = (td_CTM25 + td_CTM26)/((td_CTM25/next_td_CTM09)+(td_CTM26/td_CTM09));
                var new_td_CTM10 = (td_CTM25 + td_CTM26)/((td_CTM25/next_td_CTM10)+(td_CTM26/td_CTM10));
                var new_td_CTM11 = (td_CTM25 + td_CTM26)/((td_CTM25/next_td_CTM11)+(td_CTM26/td_CTM11));

                td_CTM02 = new_td_CTM02;
                td_CTM03 = new_td_CTM03;
                td_CTM04 = new_td_CTM04;
                td_CTM05 = new_td_CTM05;
                td_CTM06 = new_td_CTM06;
                td_CTM07 = new_td_CTM07;
                td_CTM08 = new_td_CTM08;
                td_CTM09 = new_td_CTM09;
                td_CTM10 = new_td_CTM10;
                td_CTM11 = new_td_CTM11;

                td_CTM13 = next_td_CTM13;

                // console.log()


            }


        }

        td_CTM36 = td_CTM20/td_CTM11;
        if(td_CTM36 == Infinity){
            td_CTM36 = 0;
        }

        td_CTM37 = td_CTM20/td_CTM07;

        if(td_CTM37 == Infinity){
            td_CTM37 = td_CTM22;
        }

        // td_CTM37 = Number((td_CTM37).toFixed(5));
        td_CTM38 = td_CTM38+td_CTM20;
        td_CTM39 = '';
        td_CTM40 = '';
        td_CTM41 = '';

    // sum
        sum_td_CTM36 = (sum_td_CTM36 + td_CTM36)*1;
        sum_td_CTM37 = (sum_td_CTM37 + td_CTM37)*1;

    // end sum
        var html = '<tr class="tr_'+(numb)+'">\n' +
            '                    <td class="td_CTM00">'+numb+'</td>\n' +
            '                    <td class="td_CTM01">'+td_name+'</td>\n' +
            '                    <td class="td_CTM02">'+td_CTM02+'</td>\n' +
            '                    <td class="td_CTM03">'+td_CTM03+'</td>\n' +
            '                    <td class="td_CTM04">'+td_CTM04+'</td>\n' +
            '                    <td class="td_CTM05">'+td_CTM05+' | '+td_CTM06+'</td>\n' +
            '                    <td class="td_CTM07">'+td_CTM07+' | '+td_CTM08+'</td>\n' +
            '                    <td class="td_CTM09">'+td_CTM09+' | '+td_CTM10+'</td>\n' +
            '                    <td class="td_CTM11">'+td_CTM11+'</td>\n' +
            '                    <td class="td_CTM12"></td>\n'+
            '                    <td class="td_CTM13">'+td_CTM13+'</td>\n' +
            '                    <td class="td_CTM14">'+td_CTM14+'</td>\n' +
            '                    <td class="td_CTM15"></td>\n' +
            '                    <td class="td_CTM16">'+td_CTM16+'</td>\n' +
            '                    <td class="td_CTM17">'+td_CTM17+'</td>\n' +
            '                    <td class="td_CTM18">'+td_CTM18+'</td>\n' +
            '                    <td class="td_CTM19">'+td_CTM19+'</td>\n' +
            '                    <td class="td_CTM20">'+td_CTM20+'</td>\n' +
            '                    <td class="td_CTM21">'+td_CTM21+'</td>\n' +
            '                    <td class="td_CTM22">'+td_CTM22+'</td>\n' +
            '                    <td class="td_CTM23">'+td_CTM23+'</td>\n' +
            '                    <td class="td_CTM24">'+td_CTM24+'</td>\n' +
            '                    <td class="td_CTM25">'+td_CTM25+'</td>\n' +
            '                    <td class="td_CTM26">'+td_CTM26+'</td>\n' +
            '                    <td class="td_CTM27">'+td_CTM27+'</td>\n' +
            '                    <td class="td_CTM28">'+td_CTM28+'</td>\n' +
            '                    <td class="td_CTM29">'+td_CTM29+'</td>\n' +
            '                    <td class="td_CTM30"></td>\n' +
            '                    <td class="td_CTM31"></td>\n' +
            '                    <td class="td_CTM32"></td>\n' +
            '                    <td class="td_CTM33"></td>\n' +
            '                    <td class="td_CTM34"></td>\n' +
            '                    <td class="td_CTM35"></td>\n' +
            '                    <td class="td_CTM36">'+td_CTM36+'</td>\n' +
            '                    <td class="td_CTM37">'+td_CTM37+'</td>\n' +
            '                    <td class="td_CTM38">'+td_CTM38+'</td>\n' +
            '                    <td class="td_CTM39">'+td_CTM39+'</td>\n' +
            '                    <td class="td_CTM40">'+td_CTM40+'</td>\n' +
            '                    <td class="td_CTM41">'+td_CTM41+'</td>\n' +
            '                    <td class="td_CTM42"></td>\n' +
            '                </tr>';




        $('#calk-tbody').append(html)
    }


    // var AP49 = 0.114942528735632;
    var AP49 = 0.1149425;
    var AP40 = ($("#AllTbodyWall .td40").val())*1;
    // var AR49 = 20;
    var AR49 = ($("#room_type").val())*1;
    var AR58 = ($("#AllTbodyWall .td37").val())*1;
    var AP58 = sum_td_CTM37;
    var AP50 = AP49;
    var AO58 = sum_td_CTM36;
    var AS58 = AP50;
    var G229 = 322.5;
    var G63 = 1134.75152376027;

    $('.calc_tbody_0 .tr_0 .td_CTM37').text(AP49);
    $('.calc_tbody_0 .tr_0 .td_CTM39').text(AR49);

    // setTimeout(function() {

    // W#7gxgHvxbcR%UVq
    for (var i=1;i<=numb;i++){
        var element_tr = $('.tr_'+i);


        var val_td_CTM11 = $(element_tr).find('.td_CTM11').text();
        var val_td_CTM07 = $(element_tr).find('.td_CTM07').text();
        var val = val_td_CTM07.split('|');
        val_td_CTM07 = val[0];
        val_td_CTM11 = val_td_CTM11*1;
        val_td_CTM07 = val_td_CTM07*1;

        var val_td_CTM37 = $(element_tr).find('.td_CTM37').text();
        val_td_CTM37 = val_td_CTM37*1;

        var data_td_39 = AR49-((AR49-AR58)/AP58)*AP50;
        // data_td_39 =  Number((data_td_39).toFixed(5));
        var data_td_40 = AR49-((AR49-AP40)/AP58)*AP50;
        var data_td_41 = (5330*(AO58*(AR49-AS58))/(AP58*(G63-G229)))*(val_td_CTM11/val_td_CTM07);
        var data_td_42 = '-';
        $(element_tr).find('.td_CTM39').text(data_td_39);
        $(element_tr).find('.td_CTM40').text(data_td_40);
        $(element_tr).find('.td_CTM41').text(data_td_41);


        // console.log(val_td_CTM37);
        // console.log(AP50);

        AP50 = (AP50 + val_td_CTM37)*1;

        // console.log(AP50);

        if(data_td_41 < 17.9){
            data_td_42=-17.4806*(Math.log(2.018*data_td_41-0.886))+83.124

        }else if(data_td_41 > 1638){
            data_td_42=-11.498*(Math.log(data_td_41-10.32))+50.116;
        }else{
            var test_ind1 = '-';
            var test_ind2 = '-';
            var test_val = 0;
            $.each(arr_tmy, function( index, value ) {
                if((data_td_41 >= index) && (test_ind1 == '-')){
                    test_ind1 = index;
                }


                if(data_td_41 < index){
                    test_ind2 = index;
                    test_val = value;
                }
            });

            data_td_42 = (test_ind2 - data_td_41)/(test_ind2-test_ind1)+(test_val);

        }

        $(element_tr).find('.td_CTM42').text(data_td_42);

    }

    // }, 2000);

    //
    $('.calc_tbody_last .tr_last .td_CTM36').text(AO58);
    $('.calc_tbody_last .tr_last .td_CTM37').text(AP58);
    $('.calc_tbody_last .tr_last .td_CTM39').text(AR58);
    $('.calc_tbody_last .tr_last .td_CTM40').text(AP40);


    // add data in '#AllTD'
var G60 = $('#change_coefficient').val();

    var roomType_id = $('#selectRoom').val();
    var room_section= $('#AllTbody .roomType_'+roomType_id);

    // =1.64*10^11*(EXP(-5330/(273+G216)))
var G216 = ((room_section).parent().next('tr').find('td:nth-child(2)').text())*1;


    var E220 = (1.64*(Math.pow(10, 11))*(Math.exp(-5330/(273+G216))));

// var E220 =2063.18458865503;


var E221 =431.711660736157;

        var G228 = ((room_section).parent().next('tr').next('tr').find('td:nth-child(2)').text())*1;
        var F230 =(G228/100)*E220;

    // setTimeout(function () {

    var td_html = '<tr><td>TD100</td><td>R<span class="small">o</span><span class="small" style="position: relative; top: -7px;">усл</span> - условное теплоспротивл. Констр (общее)</td><td class="TD100">'+AP58+'</td></tr>\n' +
            '    <tr><td>TD101</td><td>Выбранный коэффициент конструкции</td><td class="TD101">'+G60+'</td></tr>\n' +
            '    <tr><td>TD102</td><td>Е<span class="small">в</span> - парциальное давление (внутри)</td><td class="TD102">'+E220+'</td></tr>\n' +
            '    <tr><td>TD103</td><td>Е<span class="small">н отр</span> - парц. давл. наруж. при отриц. периоде</td><td class="TD103">'+E221+'</td></tr>\n' +
            '    <tr><td>TD104</td><td>e<span class="small">в</span> - значение парциального давления е внутри</td><td class="TD104"></td>'+F230+'</tr>\n' +
            '    <tr><td>TD105</td><td>R <span class="small">оп</span> - общее паросопротивление конструкции</td><td class="TD105">'+AO58+'</td></tr>\n' +
            '';
        $('#AllTD').html(td_html);


    // }, 2000)



    // 3-tab


    var TD100 = AP58;
    var TD101 = G60;
    var TD102 = E220;
    var TD103 = E221;
    var TD104 = F230;
    var TD105 = AO58;


    $('#notice_block').text('');
    var td100_part = AP58*2/3;
    var sum_td_CTM36 = 0;
    var isset = 0;
    var expressed_layer = false;
    var general_calculation = false;

    for (var i=1;i<=numb;i++){
        var element_tr = $('.tr_'+i);
        var val_td_CTM37 = $(element_tr).find('.td_CTM37').text();

        if(isset == 1){
            sum_td_CTM36 = sum_td_CTM36*1 + ($(element_tr).find('.td_CTM36').text())*1;
        }

        if(td100_part < val_td_CTM37){
            $(element_tr).css('background','#fcffc7');
            $('#notice_block').append('<br> Произведён анализ конструкции, выраженным слоем утеплителя назначен слой №'+i+' ');

            var val_CTM36 = ($(element_tr).find('.td_CTM36').text())*1;

            var val_td_CTM07 = $(element_tr).find('.td_CTM07').text();
            var val = val_td_CTM07.split('|');
            val_td_CTM07 = val[0];
            var val_td_CTM11 = ($(element_tr).find('.td_CTM11').text())*1;
            var layout = i;
            isset = 1;
            expressed_layer = true;

        }

    }

    if(expressed_layer == false){
        // 1. Вариант 1
        console.log('Вариант 1');
        if($('#notice_block').text().length == 0){
            $('#notice_block').append('<br> Произведён анализ конструкции, выраженного слоя утеплителя в конструкции не имеется. ');
            general_calculation = true;
        }
    }else {
        // 1. Вариант 2
        console.log('Вариант 2');

        if (val_CTM36 > sum_td_CTM36) {
            // $('#notice_block').append('По условиям п.1  найдено совпадение, переходим к пункту п.2');

            var div_val = val_td_CTM07 / val_td_CTM11;
            if (div_val > 2) {
                $('#notice_block').append('<br> ПМУ найдено на границе этого (' + layout + ') слоя и следующего (' + (layout + 1) + ') за ним. ');
                $('#notice_block').append('<br> По услвоиям п1 и п.2. найдено совпадение, "ярко выраженынй утеплитель" устанновлен, ПМУ' +
                    ' установлено на границе его поверхности между слоями №' + layout + ' И №' + (layout + 1) + ' переходим к дальнейшему расчёту без общего поиска ПМУ… ');
            } else {
                $('#notice_block').append('<br> По условиям п.1  найдено совпадение, переходим к пункту п.2 там не найдено, переходим к общему порядку расчётов. ');
                general_calculation = true;

            }

        } else {
            $('#notice_block').append('<br> По условиям п1 не найдено… переходим к общему порядку расчётов…<br>');
            general_calculation = true;


        }
    }

        if(general_calculation == true){
            // переходит к полным расчётам в раздел 2 тут в ТЗ.

            for (var i=1;i<=numb;i++){
                var element_tr = $('.tr_'+i);
                var element_tr_next = $('.tr_'+(i+1));
                var element_tr_prev = $('.tr_'+(i-1));
                var val_td_CTM42 = ($(element_tr).find('.td_CTM42').text())*1;
                var val_td_CTM42_prev = ($(element_tr_prev).find('.td_CTM42').text())*1;
                var val_td_CTM40 = ($(element_tr).find('.td_CTM40').text())*1;
                var val_td_CTM40_next = ($(element_tr_next).find('.td_CTM40').text())*1;
                var val_td_CTM40_prev = ($(element_tr_prev).find('.td_CTM40').text())*1;
                if(!isNaN(val_td_CTM42)){
                    var layout = i-1;
                    if(val_td_CTM40 < val_td_CTM42 && val_td_CTM42 < val_td_CTM40_next){
                        // пункт 2.1
                        console.log('пункт 2.1');
                        $('#notice_block').append('<br>Плоскость ПМУ найдена в слое № '+i+' ');
                        var calculation_3_1 = true;
                        var layout_3_1 = i;
                        var arr_layout = [i];

                    }else if(val_td_CTM42 > val_td_CTM40 && val_td_CTM42_prev < val_td_CTM40_prev){
                        // пункт 2.2
                        console.log('пункт 2.2');
                        $('#notice_block').append('<br>ПМУ находится между слоями №'+layout+' и №'+i+' ');
                        general_calculation = false;
                    }else{
                        // пункт 2.3.4
                        console.log('пункт 2.3');
                        general_calculation = false;
                    }

                }

            }

        }

        if(calculation_3_1 == true){

            console.log(arr_layout);
            $.each(arr_layout, function( index, value ) {
                var element_tr = $('.tr_'+value);
                var val_td_CTM14 = ($(element_tr).find('.td_CTM14').text());
                if (val_td_CTM14 == 'T') {
                    layout_3_1 = value;
                }
            });



            // переходит к расчётам по получившемуся результату в раздел 3.1
            layout = layout_3_1;
            // раздел 3.1.1
            // Необходимо установить координату Хmy плоскости этого совпадения ПМУ.
            var element_tr = $('.tr_'+layout);
            var element_tr_next = $('.tr_'+(layout+1));
            var val_td_CTM42 = ($(element_tr).find('.td_CTM42').text())*1;
            var val_td_CTM38 = ($(element_tr).find('.td_CTM38').text())*1;
            var val_td_CTM40 = ($(element_tr).find('.td_CTM40').text())*1;
            var val_td_CTM38_next = ($(element_tr_next).find('.td_CTM38').text())*1;
            var val_td_CTM40_next = ($(element_tr_next).find('.td_CTM40').text())*1;

            // =AA79+(((AB79-AD79)/(AB79-AB80))*(AA80-AA79))
            var TD120 = val_td_CTM38 + (((val_td_CTM40 - val_td_CTM42)/(val_td_CTM40 - val_td_CTM40_next))*(val_td_CTM38_next - val_td_CTM38));
            console.log('раздел 3.1.1');
            console.log('TD120 = '+TD120);



            // раздел 3.1.2
            // Находим теплосопротивление от начала стены до найденной плоскости ПМУ
            var cur_val_td_CTM37 = ($(element_tr).find('.td_CTM37').text())*1;
            var sum_td_CTM37 = 0;
            for (var i=1;i<=layout;i++){
                var element_tr = $('.tr_'+i);
                val_td_CTM37 = ($(element_tr).find('.td_CTM37').text())*1;
                sum_td_CTM37 = sum_td_CTM37 + val_td_CTM37;
            }
            sum_td_CTM37 = sum_td_CTM37 - cur_val_td_CTM37;

            // =Z76+Z77+Z78+(Z79*((AB79-AD79)/(AB79+AB80)))
            var TD121 = sum_td_CTM37 + (cur_val_td_CTM37*((val_td_CTM40 - val_td_CTM42)/(val_td_CTM40+val_td_CTM40_next)));
            console.log('раздел 3.1.2');
            console.log('TD121 = '+TD121);




            // раздел 3.1.3
            // Также надо установить Rn - сопротивление паропроницанию от внутренней поверхности в конструкции  до плоскости ПМУ
            var element_tr = $('.tr_'+layout);
            var cur_val_td_CTM36 = ($(element_tr).find('.td_CTM36').text())*1;
            var sum_td_CTM36 = 0;
            for (var i=1;i<=layout;i++){
                var element_tr = $('.tr_'+i);
                var val_td_CTM36 = ($(element_tr).find('.td_CTM36').text())*1;
                sum_td_CTM36 = sum_td_CTM36 + val_td_CTM36;
            }
            sum_td_CTM36 = sum_td_CTM36 - cur_val_td_CTM36;

            // =Y77+Y78+(Y79*((AB79-AD79)/(AB79+AB80)))
            var TD122 = sum_td_CTM36 + (cur_val_td_CTM36*((val_td_CTM40 - val_td_CTM42)/(val_td_CTM40 + val_td_CTM40_next)));
            console.log('раздел 3.1.3');
            console.log('TD122 = '+TD122);




            // раздел 3.1.4
            // Для дальнейших расчётов нам надо получить данные по материалам слоя, в котором нашли ПМУ

            var element_tr = $('.tr_'+layout);
            var val_td_CTM02 = ($(element_tr).find('.td_CTM02').text())*1;
            var val_td_CTM20 = ($(element_tr).find('.td_CTM20').text())*1;
            var val_td_CTM12 = ($(element_tr).find('.td_CTM12').text())*1;
            var TD123 = val_td_CTM02;
            var TD124 = val_td_CTM20;
            var TD125 = val_td_CTM12;
            console.log('раздел 3.1.4');
            console.log('TD123 = '+TD123);
            console.log('TD124 = '+TD124);
            console.log('TD125 = '+TD125);




            // раздел 3.1.5
            // Находим температуру в плоскости ПМУ за три периода: зимний, весенне-осенний и летний

            var TD05 = ($('#AllTbody').find('.td05').text())*1;
            var TD45 = ($('#AllTbody').find('.td45').text())*1;
            var TD48 = ($('#AllTbody').find('.td48').text())*1;
            var TD51 = ($('#AllTbody').find('.td51').text())*1;

            var TD126 = TD05 - (((TD05 - TD45)/TD100)*TD121);
            var TD127 = TD05 - (((TD05 - TD48)/TD100)*TD121);
            var TD128 = TD05 - (((TD05 - TD51)/TD100)*TD121);
            if(TD128 < TD51){
                TD128 = TD51;
            }
            console.log('раздел 3.1.5');
            console.log('TD126 = '+TD126);
            console.log('TD127 = '+TD127);
            console.log('TD128 = '+TD128);




            // раздел 3.1.6
            // Находим необходимые переменные для итоговых формул

            var TD06 = ($('#AllTbody').find('.td06').text())*1;
            var TD41 = ($('#AllTbody').find('.td41').text())*1;
            var TD42 = ($('#AllTbody').find('.td42').text())*1;
            var TD43 = ($('#AllTbody').find('.td43').text())*1;
            var TD40 = ($('#AllTbody').find('.td40').text())*1;
            var TD46 = ($('#AllTbody').find('.td46').text())*1;
            var TD49 = ($('#AllTbody').find('.td49').text())*1;
            var TD52 = ($('#AllTbody').find('.td52').text())*1;


            var TD129 = (TD06 / 100) * 1.64 * 100000000000* (Math.exp(-5330 /( 273 + TD05)));
            if(TD129 < TD43){
                TD129 = TD43;
            }
            var TD130 = 1.64 * 100000000000* (Math.exp(-5330 /( 273 + TD45)));
            var TD131 = 1.64 * 100000000000* (Math.exp(-5330 /( 273 + TD48)));
            var TD132 = 1.64 * 100000000000* (Math.exp(-5330 /( 273 + TD51)));
            var TD133 = ((TD130 * TD46) + (TD131 * TD49) + (TD132 * TD52)) / 12;
            var TD134 = 1.64 * 100000000000* (Math.exp(-5330 /( 273 + TD40)));
            var TD135 = TD105 - TD122;
            var TD136 = (0.0024 * (TD134 - TD42) * TD41) / TD135;
            console.log('раздел 3.1.6');
            console.log('TD129 = '+TD129);
            console.log('TD130 = '+TD130);
            console.log('TD131 = '+TD131);
            console.log('TD132 = '+TD132);
            console.log('TD133 = '+TD133);
            console.log('TD134 = '+TD134);
            console.log('TD135 = '+TD135);
            console.log('TD136 = '+TD136);




            // раздел 3.1.7
            // Получение окончательного результата вычисления по данному варианту ("при ПМУ в толще слоя")

            var TD140 = ((TD129 - TD133) * TD135) / (TD133 - TD43);
            var TD141 = (0.0024 * TD41 * (TD129 - TD134)) / ((TD123 * TD124 * TD125) + TD136);

            console.log('раздел 3.1.7');
            console.log('TD140 = '+TD140);
            console.log('TD141 = '+TD141);

        }

        var TD137 = '';
        if(general_calculation == false){
            // переходит к расчётам по получившемуся результату в раздел 3.2

            // раздел 3.2.1
            // Устанавливается координата Хму - на границе слоёв
            // =AA98+AA99
            var sum_td_CTM38 = 0;
            for (var i=1;i<=layout;i++){
                var element_tr = $('.tr_'+i);
                var val_td_CTM38 = ($(element_tr).find('.td_CTM38').text())*1;
                sum_td_CTM38 = sum_td_CTM38 + val_td_CTM38;
            }
            var TD120 = sum_td_CTM38;
            console.log('раздел 3.2.1');
            console.log('TD120 = '+TD120);




            // раздел 3.2.2
            // Находим теплосопротивление от начала стены до найденной плоскости ПМУ
            // =Z97+Z98+Z99
            var sum_td_CTM37 = 0;
            for (var i=1;i<=layout;i++){
                var element_tr = $('.tr_'+i);
                var val_td_CTM37 = ($(element_tr).find('.td_CTM37').text())*1;
                sum_td_CTM37 = sum_td_CTM37 + val_td_CTM37;
            }
            var TD121 = sum_td_CTM37;
            console.log('раздел 3.2.2');
            console.log('TD121 = '+TD121);




            // раздел 3.2.3
            // Также надо установить Rn - сопротивление паропроницанию от внутренней поверхности в конструкции  до плоскости ПМУ
            // =Y98+Y99
            var sum_td_CTM36 = 0;
            for (var i=1;i<=layout;i++){
                var element_tr = $('.tr_'+i);
                var val_td_CTM36 = ($(element_tr).find('.td_CTM36').text())*1;
                sum_td_CTM36 = sum_td_CTM36 + val_td_CTM36;
            }
            var TD122 = sum_td_CTM36;
            console.log('раздел 3.2.3');
            console.log('TD122 = '+TD122);




            // раздел 3.2.4
            // Для дальнейших расчётов нам надо получить данные по материалам между слоёв, в котором нашли ПМУ
            var element_tr = $('.tr_'+layout);
            var val_td_CTM02 = ($(element_tr).find('.td_CTM02').text())*1;
            var val_td_CTM20 = ($(element_tr).find('.td_CTM20').text())*1;
            var val_td_CTM12 = ($(element_tr).find('.td_CTM12').text())*1;
            var TD123_1 = val_td_CTM02;
            var TD124_1 = val_td_CTM20;
            var TD125_1 = val_td_CTM12;

            var element_tr = $('.tr_'+(layout+1));
            var val_td_CTM02 = ($(element_tr).find('.td_CTM02').text())*1;
            var val_td_CTM20 = ($(element_tr).find('.td_CTM20').text())*1;
            var val_td_CTM12 = ($(element_tr).find('.td_CTM12').text())*1;
            var TD123_2 = val_td_CTM02;
            var TD124_2 = val_td_CTM20;
            var TD125_2 = val_td_CTM12;

            TD137 = ( TD123_1 * ( TD124_1  /  2  ) * TD125_1 ) +  ( TD123_2 * ( TD124_2  /  2  ) * TD125_2 );

            console.log('раздел 3.2.4');
            console.log('TD123_1 = '+TD123_1);
            console.log('TD124_1 = '+TD124_1);
            console.log('TD125_1 = '+TD125_1);
            console.log('TD123_2 = '+TD123_2);
            console.log('TD124_2 = '+TD124_2);
            console.log('TD125_2 = '+TD125_2);




            // раздел 3.2.5
            // Находим температуру в плоскости ПМУ за три периода: зимний, весенне-осенний и летний

            var TD05 = ($('#AllTbody').find('.td05').text())*1;
            var TD45 = ($('#AllTbody').find('.td45').text())*1;
            var TD48 = ($('#AllTbody').find('.td48').text())*1;
            var TD51 = ($('#AllTbody').find('.td51').text())*1;

            var TD126 = TD05 - (((TD05 - TD45)/TD100)*TD121);
            var TD127 = TD05 - (((TD05 - TD48)/TD100)*TD121);
            var TD128 = TD05 - (((TD05 - TD51)/TD100)*TD121);
            if(TD128 < TD51){
                TD128 = TD51;
            }

            console.log('раздел 3.2.5');
            console.log('TD126 = '+TD126);
            console.log('TD127 = '+TD127);
            console.log('TD128 = '+TD128);




            // раздел 3.2.6
            // Находим необходимые переменные для итоговых формул
            var TD06 = ($('#AllTbody').find('.td06').text())*1;
            var TD41 = ($('#AllTbody').find('.td41').text())*1;
            var TD42 = ($('#AllTbody').find('.td42').text())*1;
            var TD43 = ($('#AllTbody').find('.td43').text())*1;
            var TD40 = ($('#AllTbody').find('.td40').text())*1;
            var TD46 = ($('#AllTbody').find('.td46').text())*1;
            var TD49 = ($('#AllTbody').find('.td49').text())*1;
            var TD52 = ($('#AllTbody').find('.td52').text())*1;

            var TD129 = (TD06 / 100) * 1.64 * 100000000000* (Math.exp(-5330 /( 273 + TD05)));
            if(TD129 < TD43){
                TD129 = TD43;
            }
            var TD130 = 1.64 * 100000000000* (Math.exp(-5330 /( 273 + TD45)));
            var TD131 = 1.64 * 100000000000* (Math.exp(-5330 /( 273 + TD48)));
            var TD132 = 1.64 * 100000000000* (Math.exp(-5330 /( 273 + TD51)));
            var TD133 = ((TD130 * TD46) + (TD131 * TD49) + (TD132 * TD52)) / 12;
            var TD134 = 1.64 * 100000000000* (Math.exp(-5330 /( 273 + TD40)));
            var TD135 = TD105 - TD122;
            var TD136 = (0.0024 * (TD134 - TD42) * TD41) / TD135;
            console.log('раздел 3.2.6');
            console.log('TD129 = '+TD129);
            console.log('TD130 = '+TD130);
            console.log('TD131 = '+TD131);
            console.log('TD132 = '+TD132);
            console.log('TD133 = '+TD133);
            console.log('TD134 = '+TD134);
            console.log('TD135 = '+TD135);
            console.log('TD136 = '+TD136);




            // раздел 3.2.7
            // Получение окончательного результата вычисления по данному варианту ("при ПМУ в толще слоя")

            var TD140 = ((TD129 - TD133) * TD135) / (TD133 - TD43);
            var TD141 = (0.0024 * TD41 * (TD129 - TD134)) / (TD137 + TD136);

            console.log('раздел 3.2.7');
            console.log('TD140 = '+TD140);
            console.log('TD141 = '+TD141);



            TD123 = TD123_1+' | '+TD123_2;
            TD124 = TD124_1+' | '+TD124_2;
            TD125 = TD125_1+' | '+TD125_2;

        }


    var td_html = '<tr><td>TD120</td><td></td><td class="TD120">'+TD120+'</td></tr>\n' +
        '    <tr><td>TD121</td><td>R <span class="small">до пму</span></td><td class="TD121">'+TD121+'</td></tr>\n' +
        '    <tr><td>TD122</td><td>R <span class="small">n</span></td><td class="TD122">'+TD122+'</td></tr>\n' +
        '    <tr><td>TD123</td><td>ρ <span class="small">w</span></td><td class="TD123"></td>'+TD123+'</tr>\n' +
        '    <tr><td>TD124</td><td>δ <span class="small">w</span></td><td class="TD124">'+TD124+'</td></tr>\n' +
        '    <tr><td>TD125</td><td>∆ <span class="small">w</span></td><td class="TD125">'+TD125+'</td></tr>\n' +
        '    <tr><td>TD126</td><td>t <span class="small">пму зима</span></td><td class="TD126">'+TD126+'</td></tr>\n' +
        '    <tr><td>TD127</td><td>t <span class="small">пму весна-осень</span></td><td class="TD127">'+TD127+'</td></tr>\n' +
        '    <tr><td>TD128</td><td>t <span class="small">пму лето</span></td><td class="TD128">'+TD128+'</td></tr>\n' +
        '    <tr><td>TD129</td><td>е <span class="small">в</span></td><td class="TD129">'+TD129+'</td></tr>\n' +
        '    <tr><td>TD130</td><td>Е <span class="small">зима</span></td><td class="TD130">'+TD130+'</td></tr>\n' +
        '    <tr><td>TD131</td><td>Е <span class="small">весна осень</span></td><td class="TD131">'+TD131+'</td></tr>\n' +
        '    <tr><td>TD132</td><td>Е <span class="small">лето</span></td><td class="TD132">'+TD132+'</td></tr>\n' +
        '    <tr><td>TD133</td><td>E <span class="small"></span></td><td class="TD133">'+TD133+'</td></tr>\n' +
        '    <tr><td>TD134</td><td>E <span class="small">0</span></td><td class="TD134">'+TD134+'</td></tr>\n' +
        '    <tr><td>TD135</td><td>R <span class="small">п,н</span></td><td class="TD135">'+TD135+'</td></tr>\n' +
        '    <tr><td>TD136</td><td>ɳ <span class="small"></span></td><td class="TD136">'+TD136+'</td></tr>\n' +
        '    <tr><td>TD137</td><td> <span class="small"></span></td><td class="TD137">'+TD137+'</td></tr>\n' +
        '    <tr><td>TD140</td><td>R <span class="small">n1</span><span class="small" style="position: relative; top: -7px;">тр</span></td><td class="TD140">'+TD140+'</td></tr>\n' +
        '    <tr><td>TD141</td><td>R <span class="small">n2</span><span class="small" style="position: relative; top: -7px;">тр</span></td><td class="TD141">'+TD141+'</td></tr>\n' +
        '';
    $('#AllTD').append(td_html);
    $('#AllTD .TD123').text(TD123);


    // 4-tab
    $('#result_block').html('<b>1. Соответствие нормируемого теплосопротивления стеновой конструкции фактическому:</b>');

    // 1.1 Находим температуру внутренней поверхности стены:
    var TD05 = ($('#AllTbody').find('.td05').text())*1;
    var CTM39 = ($('#calk-tbody .tr_1 .td_CTM39').text())*1;
    var TD115 = CTM39;
    var TD116 = TD05 - TD115;

    var TD54 = ($('#AllTbody').find('.td54').text())*1;
    var TD59 = ($('#AllTbody').find('.td59').text())*1;
    var TD03 = ($('#AllTbody').find('.td03').text())*1;
    var TD145 = TD100 * TD101 * TD03 * TD59;

    var TD117 = 0;
    // 1.2 Находим точку росы в помещении
    // if(Number.isInteger(TD115)){

            var formData = new FormData();
            formData.append('val', TD05);

            $.ajax({
                url: '/get_dew_data',
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function(){

                },
                success: function(result){
                    TD117 = result*1;
                    // console.log(result);
                }
            });

    // }else{
    //     var TD115_1 = Math.floor(TD115);
    //     var TD115_2 = Math.ceil(TD115);
    //
    //     var formData = new FormData();
    //     formData.append('val', TD115_1);
    //
    //     $.ajax({
    //         url: '/get_dew_data',
    //         type: 'POST',
    //         data: formData,
    //         cache: false,
    //         processData: false,
    //         contentType: false,
    //         beforeSend: function(){
    //
    //         },
    //         success: function(result){
    //             var TD117 = (TD115_1 - TD115)/(TD115_1 - TD115_2) + (result*1);
    //         }
    //     });
    //
    // }

    // 1.3 Необходимо в выбор прописать "Нормируемый температурный перепад, °С,"
    setTimeout(function () {
        var TD145 = TD100 * TD101 * TD03 * TD59;
    if($('#selectBuilding').val() == 1 || $('#selectBuilding').val() == 2){
        var TD118 =	4;
    }else if($('#selectBuilding').val() == 3){
        var TD118 =	4.5;
    }else{
        var TD118 =	TD05-TD117;
    }



    $('#result_block').append('<table class="table">\n' +
        '    <tr>\n' +
        '        <td>Нормируемое теплосопротивление конструкции:</td>\n' +
        '        <td>'+TD54+'</td>\n' +
        '        <td>(м·°С)/Вт</td>\n' +
        '    </tr>\n' +
        '    <tr>\n' +
        '        <td>При этом применён понижающий коэффициент:</td>\n' +
        '        <td>'+TD59+'</td>\n' +
        '        <td></td>\n' +
        '    </tr>\n' +
        '    <tr>\n' +
        '        <td>Приведённое теплосопротивление конструкции:</td>\n' +
        '        <td>'+TD145+'</td>\n' +
        '        <td>(м·°С)/Вт</td>\n' +
        '    </tr>\n' +
        '    <tr>\n' +
        '        <td>Требуемая удельная теплозащитная характеристика здания:</td>\n' +
        '        <td>0.562</td>\n' +
        '        <td>Вт/(м·°С)</td>\n' +
        '    </tr>\n' +
        '    <tr>\n' +
        '        <td>Фактическая удельная теплозащитная характеристика здания:</td>\n' +
        '        <td>0.503</td>\n' +
        '        <td>Вт/(м·°С)</td>\n' +
        '    </tr>\n' +
        '</table>');


    // 1.4 Сравниваем найденную точку росы в помещении с температурой внутренней поверхности стены и выдаём результат:

        if(TD145 < TD54){
            // 2.2.1
            $('#result_block').append('<br>Фактическое теплосопротивление  ('+TD145+') меньше нормируемого ('+TD54+').  Данная стеновая конструкция <span class="red">НЕ СООТВЕТСТВУЕТ</span> строительным нормативам.');
            $('#result_block').append('<br>Для соответствия строительным нормам следует подобрать другой состав конструкции, к примеру, увеличить толщину слоёв конструкции, применить материалы с меньшей теплопроводностью, добавить теплоизоляцию или при её наличии увеличить толщину');

        }else{
            // 2.2.2
            $('#result_block').append('<br>Фактическое теплосопротивление  ('+TD145+') равно или больше нормируемого ('+TD54+').  Данная стеновая конструкция <span class="green">СООТВЕТСТВУЕТ</span> строительным нормативам.');

        }

    // 2 Соответствие фактического теплосопротивления конструкции нормируемому
    $('#result_block').append('<br><br><b>2. Соответствие санитарно-гигиеническому требованию стеновой конструкции:</b>');



    // 2.1
    // var TD145 = TD100 * TD101 * TD03 * TD59;

    // 2.2



    $('#result_block').append('<table class="table">\n' +
        '    <tr>\n' +
        '        <td>Температура помещения:</td>\n' +
        '        <td>'+TD05+'</td>\n' +
        '        <td>°С</td>\n' +
        '    </tr>\n' +
        '    <tr>\n' +
        '        <td>Температура внутренней поверхности стены:</td>\n' +
        '        <td class="TD115">'+TD115+'</td>\n' +
        '        <td>°С</td>\n' +
        '    </tr>\n' +
        '    <tr>\n' +
        '        <td>Требуемая разница между температур. не более</td>\n' +
        '        <td class="TD118">'+TD118+'</td>\n' +
        '        <td>°С</td>\n' +
        '    </tr>\n' +
        '    <tr>\n' +
        '        <td>"Температура росы" (справочно):</td>\n' +
        '        <td class="TD117">'+TD117+'</td>\n' +
        '        <td>°С</td>\n' +
        '    </tr>\n' +
        '</table>');


        if(TD116 < TD118){
            // 1.4.2

            $('#result_block').append('<br>Разница температур помещения и внутренней поверхности стены не превышает установленное нормами значение.');
            $('#result_block').append('<br>По Санитарно-гигиеническому требованию данная стеновая конструкция <span class="green">СООТВЕТСТВУЕТ ТРЕБОВАНИЯМ</span> СП 50.13330.2012.');

        }else{
            // 1.4.1
            $('#result_block').append('<br>В данной стеновой конструкции точка росы образуется при ('+TD117+'), температура внутренней поверхности стены - ('+TD115+').');
            $('#result_block').append('<br>Допустимая разница температур между температурой внутренней поверхности стены - не более ('+TD118+').');
            $('#result_block').append('<br>Данная стеновая конструкция <span class="red">НЕ СООТВЕТСТВУЕТ ТРЕБОВАНИЯМ</span> СП 50.13330.2012.');


        }


    // 3 Результат расчёта по возможному влагонакоплению в конструкции
    $('#result_block').append('<br><br><b>3. Соответствие требованию защиты от переувлажнения стеновой  конструкции:</b>');


    $('#result_block').append('<table class="table">\n' +
        '    <tr>\n' +
        '        <td>Сопротивление паропроницанию от внутренней поверхности до плоскости максимального увлажнения:</td>\n' +
        '        <td>'+TD122+'</td>\n' +
        '        <td>(м·ч·Па)/мг</td>\n' +
        '    </tr>\n' +
        '    <tr>\n' +
        '        <td>Требуемое сопротивление паропроницанию (из условия недопустимости накопления влаги в ограждающей конструкции за годовой период)</td>\n' +
        '        <td>'+TD140+'</td>\n' +
        '        <td>(м·ч·Па)/мг</td>\n' +
        '    </tr>\n' +
        '    <tr>\n' +
        '        <td>Требуемого сопротивления паропроницанию, (из условия ограничения влаги в ограждающей конструкции за период с отрицательными средними месячными температурами наружного воздуха)</td>\n' +
        '        <td>'+TD141+'</td>\n' +
        '        <td>(м·ч·Па)/мг</td>\n' +
        '    </tr>\n' +
        '</table>');




    if(TD140 > TD141){
        var greatest_140_141 = TD140;
    }else{
        var greatest_140_141 = TD141;
    }

    if(TD122 > greatest_140_141){
        // Условие1
        $('#result_block').append('<br>Настоящий расчёт <span class="green">СООТВЕТСТВУЕТ</span> требованию недопустимости влагонакопления в данной расчитываемой конструкции');

        // Условие3
        if((TD122 - greatest_140_141) <= (TD122/10)){
            $('#result_block').append('<br>По анализу данных в вашем расчёте порог несоответствия нормативам очень близок. Во избежание возможных проблем рекомендуем немного улучшить конструкцию:');
            $('#result_block').append('<br>например, на внутреннюю часть поставить материалы с более низкой паропроницаемостью, на наружную часть - с более высокой, при наличии слоя теплоизоляции - увеличить толщину слоя');

        }

    }else{
        // Условие2
        $('#result_block').append('<br>Настоящий расчёт <span class="red">НЕ СООТВЕТСТВУЕТ</span> требованию недопустимости влагонакопления в данной расчитываемой конструкции. Конструкцию необходимо изменить, т.к. стена будет отсыревать.');
    }

        // $('#result_block').find('.TD117').text(TD117);
        // console.log(TD117);
    }, 3000);
    $('#AllTD .TD104').text(TD104);
    //
    // console.log(TD137);
    // console.log('TD123_1='+TD123_1);
    // console.log('TD124_1='+TD124_1);
    // console.log('TD125_1='+TD125_1);
    // console.log('TD123_2='+TD123_2);
    // console.log('TD124_2='+TD124_2);
    // console.log('TD125_2='+TD125_2);
    // TD137 = ( TD123_1 * ( TD124_1  /  2  ) * TD125_1 ) +  ( TD123_2 * ( TD124_2  /  2  ) * TD125_2 )

});

