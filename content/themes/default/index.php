<?php $this->theme->header(); ?>
    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <?php $this->theme->sidebar(); ?>

            <div class="col-md-9">
                <?php
                $regions = Data::getRegions();
                $cities = array();
                $buildings = Data::getBuildings();
                $rooms = Data::getRooms();
//                echo "<pre>";
//                print_r($regions);
//                echo "</pre>";
                ?>

<div class="option-block">
                <div class="col-md-4">
<!--                    <label for="validationCustom03">Region:</label>-->
                    <select class="form-control form-control-lg" name="region" id="selectRegion" onchange="set.ChangeRegion()" required>
                        <option value="">Выберите регион </option>
                        <?php
                        foreach($regions as $region){
                        ?>
                        <option value="<?=$region->id?>"><?=$region->name?></option>

                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-4">
<!--                    <label for="validationCustom03">City:</label>-->
                    <select class="form-control form-control-lg" name="city" id="selectCity"  onchange="set.ChangeCity()">
                        <option value="">Выберите город </option>
                        <?php
                        foreach($cities as $city){
                        ?>
                        <option value="<?=$city->id?>"><?=$city->name?></option>

                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-4">
<!--                    <label for="validationCustom03">Building:</label>-->
                    <select class="form-control form-control-lg" name="building"  id="selectBuilding"  onchange="set.ChangeBuilding()">
                        <option value="">Выберите тип здания </option>
                        <?php
                        foreach($buildings as $building){
                        ?>
                        <option value="<?=$building->id?>"><?=$building->name?></option>

                        <?php
                        }
                        ?>
                    </select>
                </div>
<!--                <div class="col-md-3">-->
<!--                    <label for="validationCustom03">City:</label>-->
<!--                    <select class="form-control form-control-lg" name="room"  id="selectRoom"  onchange="set.ChangeRoom()">-->
<!--                        <option value="">Выберите тип помещения </option>-->
<!--                        --><?php
//                        foreach($rooms as $room){
//                        ?>
<!--                        <option value="--><?//=$room->id?><!--">--><?//=$room->name?><!--</option>-->
<!---->
<!--                        --><?php
//                        }
//                        ?>
<!--                    </select>-->
<!--                </div>-->

</div>
                <div class="panel-heading">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li class="nav-item active">
                            <a class="nav-link active" href="#Result" data-toggle="tab">Результат</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#City" data-toggle="tab">Город</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Building" data-toggle="tab">Тип здания</a>
                        </li>
<!--                        <li class="nav-item">-->
<!--                            <a class="nav-link" href="#Room" data-toggle="tab">Тип помещения</a>-->
<!--                        </li>-->

                    </ul>
                </div>

    <div class="panel-body">
        <div class="tab-content">

            <div class="tab-pane fade in active" id="Result">

                <div class="row">
                    <div class="col project-title">
<!--                        <h3>-->
<!--                            Результат-->
<!--                        </h3>-->
                    </div>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th style="text-align: center">Value</th>
                    </tr>
                    </thead>
                    <tbody id="AllTbody">
<!-- ==============================================-->

<!--==============================================-->
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="City">


                <div class="row">
                    <div class="col project-title">
<!--                        <h3>-->
<!--                            Город-->
<!--                        </h3>-->
                    </div>
                </div>

                <table class="table">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Value</th>
                    </tr>
                    </thead>
                    <tbody id="cityTbody">

                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="Building">


                <div class="row">
                    <div class="col project-title">
<!--                        <h3>-->
<!--                            Тип здани-->
<!--                        </h3>-->
                    </div>
                </div>

                <table class="table">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Value1</th>
                        <th>Value2</th>
                    </tr>
                    </thead>
                    <tbody id="buildingTbody">

                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="Room">


                <div class="row">
                    <div class="col project-title">
<!--                        <h3>-->
<!--                            Тип помещения-->
<!--                        </h3>-->
                    </div>
                </div>

                <table class="table">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Value</th>
                    </tr>
                    </thead>
                    <tbody id="roomTbody">

                    <tr>
                        <th scope="row">

                        </th>
                        <td>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
            </div>
        </div>
    </div>

<?php $this->theme->footer(); ?>