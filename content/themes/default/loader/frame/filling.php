<?php

$par_filling['col'] = 'COL37';
$par_filling['val'] = 'ЗК';
$par_filling['operator'] = '=';
$material_filling = Data::getMaterialsByParams($par_filling);
?>
<li class="col-md-12 margin-li li_<?=$_GET['count'];?>" data-id="<?=$_GET['count'];?>">
    <div>каркасная конструкция</div>
    <div class="col-md-6 first_select">
        <select class="form-control form-control-lg" name="mat_cat" onchange="set.ChangeMaterialCatBlocks(this)">
            <?php foreach($material_filling as $item){ ?>
                <option value="<?=$item->id?>"><?=$item->name?></option>
            <?php } ?>
        </select>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control thickness" value="">
    </div>

    <button class="edit btn btn-danger"  onclick="editField(this)"><i class="glyphicon glyphicon-edit"></i></button>
    <?php
    if($_GET['isset'] != 'html') {
        ?>
        <button class="up btn btn-danger" onclick="upField(this)"><i class="glyphicon glyphicon-arrow-up"></i></button>
        <button class="down btn btn-danger" onclick="downField(this)"><i class="glyphicon glyphicon-arrow-down"></i>
        </button>
        <?php
    }
    ?>
    <button class="remove btn btn-danger" onclick="removeField(this)"><i class="glyphicon glyphicon-remove"></i></button>
    <div class="edit-block">
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="save">
            </div>
        </div>
    </div>

<!--</li>-->
<?php

$par_frame['col'] = 'COL36';
$par_frame['val'] = 'К';
$par_frame['operator'] = '=';
$material_frame = Data::getMaterialsByParams($par_frame);
?>
    <div class="col-md-6 second_select">
        <select class="form-control form-control-lg" name="mat_cat"  id="selectMat"  onchange="set.ChangeMaterialCatBlocks(this)">
            <?php foreach($material_frame as $item){ ?>
                <option value="<?=$item->id?>"><?=$item->name?></option>
            <?php } ?>
        </select>
    </div>

    <div class="col-md-6 col-md-offset-6 data-input-block frame-input">
        <div class="col-md-8">
            <label for="">расстояние между стойками</label>
        </div>
        <div class="col-md-4">
            <input type="text" class="form-control frame-between" value="">
        </div>
        <div class="col-md-8">
            <label for="">ширина стоек каркаса </label>
        </div>
        <div class="col-md-4">
            <input type="text" class="form-control frame-width" value="">
        </div>
    </div>


    <div class="edit-block inline-block">
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="save">
            </div>
        </div>
    </div>

</li>