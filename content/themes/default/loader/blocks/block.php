<?php

$par_seam['col'] = 'COL34';
$par_seam['val'] = 'Б';
$par_seam['operator'] = '=';
$material_seam = Data::getMaterialsByParams($par_seam);
?>
<li class="col-md-12 margin-li li_<?=$_GET['count'];?>" data-id="<?=$_GET['count'];?>">

    <div>кладка из блоков</div>
    <div class="col-md-6 first_select">
        <select class="form-control form-control-lg" name="mat_cat" onchange="set.ChangeMaterialCatBlocks(this)">
            <?php foreach($material_seam as $item){ ?>
                <option value="<?=$item->id?>"><?=$item->name?></option>
            <?php } ?>
        </select>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control thickness" value="">
    </div>

    <button class="edit btn btn-danger"  onclick="editField(this)"><i class="glyphicon glyphicon-edit"></i></button>
    <?php
    if($_GET['isset'] != 'html') {
        ?>
        <button class="up btn btn-danger" onclick="upField(this)"><i class="glyphicon glyphicon-arrow-up"></i></button>
        <button class="down btn btn-danger" onclick="downField(this)"><i class="glyphicon glyphicon-arrow-down"></i>
        </button>
        <?php
    }
    ?>
    <button class="remove btn btn-danger" onclick="removeField(this)"><i class="glyphicon glyphicon-remove"></i></button>
    <div class="edit-block">
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="save">
            </div>
        </div>
    </div>

<?php

$par_block['col'] = 'COL35';
$par_block['val'] = 'Ш';
$par_block['operator'] = '=';
$material_block = Data::getMaterialsByParams($par_block);
?>
    <div class="col-md-6 second_select">
        <select class="form-control form-control-lg" name="mat_cat"  id="selectMat"  onchange="set.ChangeMaterialCatBlocks(this)">
            <?php foreach($material_block as $item){ ?>
                <option value="<?=$item->id?>"><?=$item->name?></option>
            <?php } ?>
        </select>
    </div>

    <div class="col-md-6 col-md-offset-6 data-input-block block-input">
        <div class="col-md-8">
            <label for="">Размер блока(ш-в)</label>
        </div>
        <div class="col-md-4">
            <input type="text" class="form-control block-w" value="">
            <input type="text" class="form-control block-h" value="">
        </div>
        <div class="col-md-8">
            <label for="">Толщина шва </label>
        </div>
        <div class="col-md-4">
            <input type="text" class="form-control block-seam" value="">
        </div>
    </div>


    <div class="edit-block inline-block">
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="save">
            </div>
        </div>
    </div>

</li>