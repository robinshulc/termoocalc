<?php

$par_holder['col'] = 'COL17';
$par_holder['val'] = 'H';
$par_holder['operator'] = '=';
$material_holder = Data::getMaterialsByParams($par_holder);
?>
<li class="col-md-12 margin-li li_<?=$_GET['count'];?>" data-id="<?=$_GET['count'];?>">
    <div class="col-md-6">
        <select class="form-control form-control-lg" name="mat_cat" onchange="set.ChangeMaterialCat(this)">
            <?php foreach($material_holder as $item){ ?>
                <option value="<?=$item->id?>"><?=$item->name?></option>
            <?php } ?>
        </select>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control thickness" value="">
    </div>

    <button class="edit btn btn-danger"  onclick="editField(this)"><i class="glyphicon glyphicon-edit"></i></button>
    <button class="remove btn btn-danger" onclick="removeField(this)"><i class="glyphicon glyphicon-remove"></i></button>
    <div class="edit-block">
        <div class="col-md-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="save">
            </div>
        </div>
    </div>

</li>

