<?php $this->theme->header(); ?>
<?php
$regions = Data::getRegions();
$cities = array();
$buildings = Data::getBuildings();
$rooms = Data::getRooms();
$material_category = Data::getMaterialCategory();

$par_homogeneous['col'] = 'COL18';
$par_homogeneous['val'] = 'T';
$par_homogeneous['operator'] = '!=';
$material_homogeneous = Data::getMaterialsByParams($par_homogeneous);
$par_thermal['col'] = 'COL18';
$par_thermal['val'] = 'T';
$par_thermal['operator'] = '=';
$material_thermal = Data::getMaterialsByParams($par_thermal);
$par_holder['col'] = 'COL17';
$par_holder['val'] = 'H';
$par_holder['operator'] = '=';
$material_holder = Data::getMaterialsByParams($par_holder);


$coefficient_list = Data::getCoefficientList();
$cities = array();

?>
    <!-- Main Content -->
    <div class="container">
        <div class="row">
<?php $this->theme->sidebar(); ?>


            <div class="col-md-9">
                <h1>Ввод данных</h1>
                <span class="small">* - Обязательные поля</span>
                <div class="calc-block">

                    <div class="calc-places select">
                        <div class="col-md-4">
                        <span>Код</span>
                        <select class="form-control form-control-lg" name="" id="">
                            <option value=""></option>
                            <option value=""></option>
                            <option value=""></option>
                        </select>
                        </div>
                        <div class="col-md-4">
                        <span>Регион</span>
                            <select class="form-control form-control-lg" name="region" id="selectRegion" onchange="set.ChangeRegion()" required>
                                <option value="">Выберите регион </option>
                            <?php foreach($regions as $region){ ?>
                                <option value="<?=$region->id?>"><?=$region->name?></option>
                            <?php } ?>
                        </select>
                        </div>
                        <div class="col-md-4">
                        <span>Город</span>
                            <select class="form-control form-control-lg" name="city" id="selectCity"  onchange="set.ChangeCityWallPage()">
                                <option value="">Выберите город </option>
                                <?php
                                foreach($cities as $city){
                                    ?>
                                    <option value="<?=$city->id?>"><?=$city->name?></option>

                                    <?php
                                }
                                ?>
                        </select>
                        </div>
                    </div>

                    <div class="calc-type-build select">
                        <span>Тип Здания*</span>
                        <select class="form-control form-control-lg" name="building"  id="selectBuilding"  onchange="set.ChangeBuildingWallPage()">
<!--                            <option value="">Выберите тип здания </option>-->
                            <?php foreach($buildings as $building){ ?>
                                <option value="<?=$building->id?>"><?=$building->name?></option>
                            <?php } ?>
                        </select>


                    </div>
                    <span>Тип помещения*</span>
                    <div class="calc-type-room select col-sm-10">
                        <select class="form-control form-control-lg" name=""  id="selectRoom" disabled onchange="set.ChangeRoomType()">
                            <?php foreach($rooms as $room){ ?>
                                <option value="<?=$room->id?>"><?=$room->name?></option>
                            <?php } ?>
                        </select>

                    </div>

                    <div class="col-sm-2">
                        <input class="form-control" type="text" id="room_type" disabled value="20">
                    </div>


                    <div class="calc-coefficient input margin-top">
                        <div>Коэффициенты m<span class="small">p</span></div>
                        <div class="col-md-4">
                            <label>m<span class="small">p стена</span></label>
                            <input class="form-control" type="text" value="1">
                        </div>
                        <div class="col-md-4">
                        <label>m<span class="small">p окна</span></label>
                        <input class="form-control" type="text" value="1">
                        </div>
                        <div class="col-md-4">
                        <label>m<span class="small">р прочие</span></label>
                        <input class="form-control" type="text" value="1">
                        </div>

            </div>

<!--++++++++++++++++++++++++++++++++++++++++++++++++++-->
                    <div id="AllTbodyWall">
                        <input type="hidden" class="td37">
                        <input type="hidden" class="td40">
                        <input type="hidden" class="a_or_b">
                    </div>
<!--++++++++++++++++++++++++++++++++++++++++++++++++++-->


                    <h4 class="margin-top inline-block">Выбор слоев <span class="red">(выбранные материалы отображаются на листе Ввод данных (материалы))</span> </h4>
                    <ol>
                        <li>
                            <div><span>Стены</span></div>
                            <ol id="ol">
                                <div>Внутренний слой</div>


<!--                                =========================================================-->


                                <div class="col-md-12" style="background-color: aliceblue; padding: 20px 32px;">
                                <div><span>Материалы до несущей стены</span></div>

                                    <div class="add-fields block_1">
                                        <input id="block_1_input" type="hidden" value="1" disabled>

                                        <li class="col-md-12 margin-li li_1" data-id="1">
                                            <div class="col-md-6">
                                            <select class="form-control form-control-lg" name="mat_cat" onchange="set.ChangeMaterialCat(this)">
                                                <?php foreach($material_homogeneous as $item){ ?>
                                                    <option value="<?=$item->id?>"><?=$item->name?></option>
                                                <?php } ?>
                                            </select>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control thickness" value="">
                                            </div>

                                            <button class="edit btn btn-danger"  onclick="editField(this)"><i class="glyphicon glyphicon-edit"></i></button>
                                            <button class="up btn btn-danger"  onclick="upField(this)"><i class="glyphicon glyphicon-arrow-up"></i></button>
                                            <button class="down btn btn-danger"  onclick="downField(this)"><i class="glyphicon glyphicon-arrow-down"></i></button>
                                            <button class="remove btn btn-danger" onclick="removeField(this)"><i class="glyphicon glyphicon-remove"></i></button>
                                            <div class="edit-block">
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="submit" class="btn btn-success" value="save">
                                                    </div>
                                                </div>
                                            </div>

                                        </li>

                                    </div>
                                    <div class="controls" id="fields">
                                        <form role="form" autocomplete="off">
                                            <a href="javascript:void(0)" class="add-field homogeneous">+ Добавить слой материала</a>
                                        </form>
                                    </div>

                                    <div class="controls">
                                        <form role="form" autocomplete="off">
                                            <a href="javascript:void(0)" class="add-field thermal">+ Добавить слой теплоизоляции</a>
                                        </form>
                                    </div>

                                    <div class="controls">
                                        <form role="form" autocomplete="off">
                                            <a href="javascript:void(0)" class="add-field blocks">+ Добавить кладку из блоков </a>
                                        </form>
                                        <div class="opt-block">
                                            <form role="form" autocomplete="off">
                                            <a href="javascript:void(0)" class="blocks-block">БЛОКИ</a>
                                            </form>

                                            <form role="form" autocomplete="off">
                                            <a href="javascript:void(0)"  class="blocks-seam">ШВЫ</a>
                                            </form>

                                        </div>
                                    </div>

                                    <div class="controls">
                                        <form role="form" autocomplete="off">
                                            <a href="javascript:void(0)" class="add-field frame">+ Добавить  каркасную конструкцию </a>
                                        </form>
                                        <div class="opt-frame">
                                            <form role="form" autocomplete="off">
                                                <a href="javascript:void(0)" class="frame-frame">Каркас</a>
                                            </form>

                                            <form role="form" autocomplete="off">
                                                <a href="javascript:void(0)"  class="frame-filling">Заполнение каркаса</a>
                                            </form>

                                        </div>
                                    </div>


                                </div>






                                <div class="col-md-12" style="background-color: #d8e2df;  padding: 20px 32px;">
                                <div><span>Несущая стена</span></div>
                                    <div class="add-fields block_2">
                                        <input id="block_2_input" type="hidden" value="1" disabled>


                                        <li class="col-md-12 margin-li li_2" data-id="2" >
                                            <div class="col-md-6">
                                                <select class="form-control form-control-lg" onchange="set.ChangeMaterialCat(this)">
                                                    <?php foreach($material_holder as $item){ ?>
                                                        <option value="<?=$item->id?>"><?=$item->name?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control thickness" value="">
                                            </div>

                                            <button class="edit btn btn-danger"  onclick="editField(this)"><i class="glyphicon glyphicon-edit"></i></button>
                                            <button class="remove btn btn-danger" onclick="removeField(this)"><i class="glyphicon glyphicon-remove"></i></button>
                                            <div class="edit-block">
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="submit" class="btn btn-success" value="save">
                                                    </div>
                                                </div>
                                            </div>

                                        </li>
                                    </div>
                                    <div class="controls hide-holder">
                                        <form role="form" autocomplete="off" class="holder_button">
                                            <a href="javascript:void(0)" class="add-field holder">+ Добавить несущий материала</a>
                                        </form>
                                    </div>

                                    <div class="controls">
                                        <form role="form" autocomplete="off" class="holder_button">
                                            <a href="javascript:void(0)" class="add-field blocks">+ Добавить кладку из блоков </a>
                                        </form>
                                    </div>

                                    <div class="controls">
                                        <form role="form" autocomplete="off" class="holder_button">
                                            <a href="javascript:void(0)" class="add-field frame">+ Добавить  каркасную конструкцию </a>
                                        </form>
                                    </div>
                                </div>






                                <div class="col-md-12" style="background-color: aliceblue; padding: 20px 32px;">
                                <div><span>Материалы после несущей стены</span></div>

                                    <div class="add-fields block_3">
                                        <input id="block_3_input" type="hidden" value="1" disabled>


                                        <li class="col-md-12 margin-li li_3"  data-id="3">
                                            <div class="col-md-6">
                                                <select class="form-control form-control-lg" name="mat_cat" onchange="set.ChangeMaterialCat(this)">
                                                    <?php foreach($material_homogeneous as $item){ ?>
                                                        <option value="<?=$item->id?>"><?=$item->name?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control thickness" value="">
                                            </div>

                                            <button class="edit btn btn-danger"  onclick="editField(this)"><i class="glyphicon glyphicon-edit"></i></button>
                                            <button class="up btn btn-danger"  onclick="upField(this)"><i class="glyphicon glyphicon-arrow-up"></i></button>
                                            <button class="down btn btn-danger"  onclick="downField(this)"><i class="glyphicon glyphicon-arrow-down"></i></button>
                                            <button class="remove btn btn-danger" onclick="removeField(this)"><i class="glyphicon glyphicon-remove"></i></button>
                                            <div class="edit-block">
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="submit" class="btn btn-success" value="save">
                                                    </div>
                                                </div>
                                            </div>

                                        </li>


                                    </div>

                                    <div class="controls" id="fields">
                                        <form role="form" autocomplete="off">
                                            <a href="javascript:void(0)" class="add-field homogeneous">+ Добавить слой материала</a>
                                        </form>
                                    </div>

                                    <div class="controls">
                                        <form role="form" autocomplete="off">
                                            <a href="javascript:void(0)" class="add-field thermal">+ Добавить слой теплоизоляции</a>
                                        </form>
                                    </div>

                                    <div class="controls">
                                        <form role="form" autocomplete="off">
                                            <a href="javascript:void(0)" class="add-field blocks">+ Добавить кладку из блоков </a>
                                        </form>

                                    </div>

                                    <div class="controls">
                                        <form role="form" autocomplete="off">
                                            <a href="javascript:void(0)" class="add-field frame">+ Добавить  каркасную конструкцию </a>
                                        </form>

                                    </div>
                                </div>




<!--                                ==========================================================-->




                                <div style="display: inline-block;">
                                    <div><span>Наружный слой</span></div>

                                    <div class="col-md-8">
                                        <label for="">Тип конструкции стены</label>

                                        <select class="form-control form-control-lg" name="coefficient"  id="selectCoefficient"  onchange="set.ChangeCoefficient()">
                                            <?php foreach($coefficient_list as $item){ ?>
                                                <option value="<?=$item->id?>"><?=$item->name?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">коэффициент</label>
                                        <input class="form-control" id="change_coefficient" disabled type="text" value="1">
                                    </div>
                                </div>
                            </ol>
                        </li>
                    </ol>

                    <div class="margin-top" id="Data">

<ol id="tab-ol">


    <div class="row">
        <div class="col project-title">
            <h3>
                Data <span id="data"></span>
            </h3>
        </div>
    </div>

    <div id="dataTbody">
        <?php
        echo '<div class="table_block_1">';
        include 'loader/table_block_1.php';
        echo '</div>';
        echo '<div class="table_block_2">';
        include 'loader/table_block_2.php';
        echo '</div>';
        echo '<div class="table_block_3">';
        include 'loader/table_block_3.php';
        echo '</div>';
        ?>
    </div>


</ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="button-block">
        <button id="calculate" class="btn btn-primary pull-right margin-top" disabled>Calculate</button>
        </div>
        <div class="row calc">
            <div class="margin-top table-calculate">
                <span class="padding-20 display-block calc-span"><h3>Справочник по применяемым материалам</h3></span>
                <table class="table">
        <thead>
                <tr>
                    <th colspan="2" rowspan="3"></th>
                    <th colspan="3">Характеристики материалов в сухом состоянии</th>
                    <th colspan="4">Расчетные характеристики материалов при условиях эксплуатации конструкций А и Б </th>
                    <th rowspan="3">Предельно допустимое приращение влаги, %</th>
                    <th rowspan="3">КОД обозначения что материал НЕСУЩИЙ</th>
                    <th rowspan="3">КОД обозначения что материал ТЕПЛОИЗОЛЯЦИ</th>
                    <th rowspan="3">резерв1</th>
                    <th rowspan="3">Фиксированная толщина слоя в мм</th>
                    <th rowspan="3">Сопротивление паропроницанию  , м ·ч·Па/мг</th>
                    <th rowspan="3">Источник данных</th>
                    <th rowspan="3">весённая клиентом толщина слоя в мм</th>
                    <th rowspan="3">весённая клиентом толщина слоя переводится в метры (после ввода умножается на 0,001)</th>
                    <th colspan="4">Термическое сопротивление замкнутой воздушной прослойки,м2 · °С/Вт</th>
                    <th rowspan="3">Шаг обрешётки каркаса, мм</th>
                    <th rowspan="3">Ширина стоек каркаса, мм</th>
                    <th rowspan="3">Размер блока кладки ширина, мм</th>
                    <th rowspan="3">Размер блока кладки высота, мм</th>
                    <th rowspan="3">Толщина шва  кладки, мм</th>
                    <th rowspan="3">резерв1</th>
                    <th rowspan="3">резерв1</th>
                    <th rowspan="3">резерв1</th>
                    <th rowspan="3">резерв1</th>
                    <th rowspan="3">резерв1</th>
                    <th rowspan="3">резерв1</th>
                    <th rowspan="3">R сопротивление паропроницанию</th>
                    <th rowspan="3">R теплосопр.слоя</th>
                    <th rowspan="3">координата х график толщины (м)</th>
                    <th rowspan="3">t слоёв при пятидневке</th>
                    <th rowspan="3">температура слоя при средней темп. Периода отрицательных температур</th>
                    <th rowspan="3">вычисление комплекса tmy </th>
                    <th rowspan="3">температура в слое</th>
                </tr>
                <tr>
                    <th rowspan="2">плотность кг/куб.м.</th>
                    <th rowspan="2">удельная теплоёмкость кДж/(кг·°С)</th>
                    <th rowspan="2">теплопроводность Вт/(м·°С)</th>
                    <th rowspan="2">влажность %</th>
                    <th rowspan="2">теплопроводность Вт/(м·°С)</th>
                    <th rowspan="2">теплоусвоение (при 24 ч) Вт/(м ·°С)</th>
                    <th rowspan="2">паропроницаемость мг/(м·ч·Па)</th>
                    <th colspan="2">горизонтальной при потоке теплоты снизу вверх и вертикальной, при температуре воздуха в прослойке</th>
                    <th colspan="2">горизонтальной при потоке теплоты сверху вниз, при температуре воздуха в прослойке</th>
                </tr>
                <tr>
                    <th>положительной</th>
                    <th>отрицательной</th>
                    <th>положительной</th>
                    <th>отрицательной</th>
                  </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>A | B</td>
                    <td>A | B</td>
                    <td>A | B</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>CTM 01</td>
                    <td>CTM 02</td>
                    <td>CTM 03</td>
                    <td>CTM 04</td>
                    <td>CTM05 | CTM06</td>
                    <td>CTM07 | CTM08</td>
                    <td>CTM09 | CTM10</td>
                    <td>CTM 11</td>
                    <td>CTM 12</td>
                    <td>CTM 13</td>
                    <td>CTM 14</td>
                    <td>CTM 15</td>
                    <td>CTM 16</td>
                    <td>CTM 17</td>
                    <td>CTM 18</td>
                    <td>CTM 19</td>
                    <td>CTM 20</td>
                    <td>CTM 21</td>
                    <td>CTM 22</td>
                    <td>CTM 23</td>
                    <td>CTM 24</td>
                    <td>CTM 25</td>
                    <td>CTM 26</td>
                    <td>CTM 27</td>
                    <td>CTM 28</td>
                    <td>CTM 29</td>
                    <td>CTM 30</td>
                    <td>CTM 31</td>
                    <td>CTM 32</td>
                    <td>CTM 33</td>
                    <td>CTM 34</td>
                    <td>CTM 35</td>
                    <td>CTM 36</td>
                    <td>CTM 37</td>
                    <td>CTM 38</td>
                    <td>CTM 39</td>
                    <td>CTM 40</td>
                    <td>CTM 41</td>
                    <td>CTM 42</td>
                </tr>
        </thead>
        <tbody class="calc_tbody_0">
                <tr class="tr_0">
                    <td class="td_CTM00">0</td>
                    <td class="td_CTM01"></td>
                    <td class="td_CTM02"></td>
                    <td class="td_CTM03"></td>
                    <td class="td_CTM04"></td>
                    <td class="td_CTM05"></td>
                    <td class="td_CTM07"></td>
                    <td class="td_CTM09"></td>
                    <td class="td_CTM11"></td>
                    <td class="td_CTM12"></td>
                    <td class="td_CTM13"></td>
                    <td class="td_CTM14"></td>
                    <td class="td_CTM15"></td>
                    <td class="td_CTM16"></td>
                    <td class="td_CTM17"></td>
                    <td class="td_CTM18"></td>
                    <td class="td_CTM19"></td>
                    <td class="td_CTM20"></td>
                    <td class="td_CTM21"></td>
                    <td class="td_CTM22"></td>
                    <td class="td_CTM23"></td>
                    <td class="td_CTM24"></td>
                    <td class="td_CTM25"></td>
                    <td class="td_CTM26"></td>
                    <td class="td_CTM27"></td>
                    <td class="td_CTM28"></td>
                    <td class="td_CTM29"></td>
                    <td class="td_CTM30"></td>
                    <td class="td_CTM31"></td>
                    <td class="td_CTM32"></td>
                    <td class="td_CTM33"></td>
                    <td class="td_CTM34"></td>
                    <td class="td_CTM35"></td>
                    <td class="td_CTM36"></td>
                    <td class="td_CTM37"></td>
                    <td class="td_CTM38"></td>
                    <td class="td_CTM39"></td>
                    <td class="td_CTM40"></td>
                    <td class="td_CTM41"></td>
                    <td class="td_CTM42"></td>
                </tr>
        </tbody>
        <tbody id="calk-tbody">
        </tbody>

        <tbody class="calc_tbody_last">

        <tr class="tr_last">
            <td class="td_CTM00">X</td>
            <td class="td_CTM01"></td>
            <td class="td_CTM02"></td>
            <td class="td_CTM03"></td>
            <td class="td_CTM04"></td>
            <td class="td_CTM05"></td>
            <td class="td_CTM07"></td>
            <td class="td_CTM09"></td>
            <td class="td_CTM11"></td>
            <td class="td_CTM12"></td>
            <td class="td_CTM13"></td>
            <td class="td_CTM14"></td>
            <td class="td_CTM15"></td>
            <td class="td_CTM16"></td>
            <td class="td_CTM17"></td>
            <td class="td_CTM18"></td>
            <td class="td_CTM19"></td>
            <td class="td_CTM20"></td>
            <td class="td_CTM21"></td>
            <td class="td_CTM22"></td>
            <td class="td_CTM23"></td>
            <td class="td_CTM24"></td>
            <td class="td_CTM25"></td>
            <td class="td_CTM26"></td>
            <td class="td_CTM27"></td>
            <td class="td_CTM28"></td>
            <td class="td_CTM29"></td>
            <td class="td_CTM30"></td>
            <td class="td_CTM31"></td>
            <td class="td_CTM32"></td>
            <td class="td_CTM33"></td>
            <td class="td_CTM34"></td>
            <td class="td_CTM35"></td>
            <td class="td_CTM36"></td>
            <td class="td_CTM37"></td>
            <td class="td_CTM38"></td>
            <td class="td_CTM39"></td>
            <td class="td_CTM40"></td>
            <td class="td_CTM41"></td>
            <td class="td_CTM42"></td>
        </tr>
        </tbody>

                </table>
            </div>

        </div>

        <div id="notice_block"></div>

        <table class="table margin-top">
            <thead>
            <tr>
                <th>TD</th>
                <th>Name</th>
                <th style="text-align: center">Value</th>
            </tr>
            </thead>
            <tbody id="AllTD">
            <!-- ==============================================-->

            <!--==============================================-->
            </tbody>
        </table>


        <table class="table margin-top td_table">
            <thead>
            <tr>
                <th>Name</th>
                <th style="text-align: center">Value</th>
            </tr>
            </thead>
            <tbody id="AllTbody">
            <!-- ==============================================-->

            <!--==============================================-->
            </tbody>
        </table>


        <div id="result_block"></div>


    </div>




<?php $this->theme->footer(); ?>
