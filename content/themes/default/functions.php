<?php
// Add css files
Asset::css('vendor/bootstrap/css/bootstrap.min');
Asset::css('css/style');
Asset::css('vendor/font-awesome/css/font-awesome.min');

// Add js scripts
Asset::js('vendor/jquery/jquery.min');
Asset::js('vendor/bootstrap/js/bootstrap.min');
Asset::js('js/jqBootstrapValidation');
Asset::js('js/contact_me');
Asset::js('js/settings');
Asset::js('js/script');
Asset::js('js/calc');