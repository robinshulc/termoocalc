<?php
/**
 * List routes
 */

$this->router->add('home', '/', 'HomeController:index');
$this->router->add('get-cities', '/get-cities', 'DataController:getCities', 'POST');
$this->router->add('get-city-params', '/get-city-params', 'DataController:getCityParams', 'POST');
$this->router->add('get-building-params', '/get-building-params', 'DataController:getBuildingParams', 'POST');
$this->router->add('get-room-params', '/get-room-params', 'DataController:getRoomParams', 'POST');
$this->router->add('get-room-type', '/get-room-type', 'DataController:getRoomType', 'POST');
$this->router->add('get-cities-buildings-params', '/get-cities-buildings', 'DataController:getCitiesBuildingsParams', 'POST');

$this->router->add('walls', '/walls', 'HomeController:walls');

$this->router->add('get-mat_cat_1', '/get-mat_cat', 'DataController:getMatCat', 'POST');
$this->router->add('get-mat_by_cat', '/get-mat_by_cat', 'DataController:getMatByCat', 'POST');
$this->router->add('get-loader-homogeneous', '/loader-homogeneous', 'HomeController:getLoaderHomogeneous');
$this->router->add('get-loader-thermal', '/loader-thermal', 'HomeController:getLoaderThermal');
$this->router->add('get-loader-blocks-block', '/loader-blocks-block', 'HomeController:getLoaderBlocksBlock');
$this->router->add('get-loader-blocks-seam', '/loader-blocks-seam', 'HomeController:getLoaderBlocksSeam');
$this->router->add('get-loader-frame-frame', '/loader-frame-frame', 'HomeController:getLoaderFrameFrame');
$this->router->add('get-loader-frame-filling', '/loader-frame-filling', 'HomeController:getLoaderFrameFilling');
$this->router->add('get-loader-holder', '/loader-holder', 'HomeController:getLoaderHolder');
$this->router->add('get-coefficient', '/get-coefficient', 'DataController:getCoefficient', 'POST');

$this->router->add('get-table-homogeneous', '/table-homogeneous_thermal', 'HomeController:getTableHomogeneousThermal');
$this->router->add('get-table-thermal', '/table-block_filling', 'HomeController:getTableBlockFilling');

$this->router->add('get-mat_cat_blocks', '/get-mat_cat_blocks', 'DataController:getMatCatBlocks', 'POST');
$this->router->add('test', '/test', 'DataController:test');
$this->router->add('get_dew_data', '/get_dew_data', 'DataController:getDewData', 'POST');


