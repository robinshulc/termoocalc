<?php

namespace Cms\Model\Building;

use Engine\Model;

class BuildingRepository extends Model
{
    /**
     * @param array $params
     * @return int
     */
    public function add($params = [])
    {
        if (empty($params)) {
            return 0;
        }

        $menu = new Building;
        $menu->setName($params['name']);
        $menuId = $menu->save();

        return $menuId;
    }

    public function getList()
    {
        $query = $this->db->query(
            $this->queryBuilder
                ->select()
                ->from('type_building')
                ->orderBy('id', 'ASC')
                ->sql()
        );

        return $query;
    }

    public function getBuilding($id)
    {
        $region = new Building($id);

        return $region->findOne();
    }


    public function getBuildingData($id)
    {
        if (empty($id)) {
            return 0;
        }

        $sql = $this->queryBuilder->select()
            ->from('type_building_data')
            ->where('type_building_id', $id)
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }
}