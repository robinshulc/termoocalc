<?php

namespace Cms\Model\Room;

use Engine\Model;

class RoomRepository extends Model
{
    /**
     * @param array $params
     * @return int
     */
    public function add($params = [])
    {
        if (empty($params)) {
            return 0;
        }

        $menu = new Room;
        $menu->setName($params['name']);
        $menuId = $menu->save();

        return $menuId;
    }

    public function getList()
    {
        $query = $this->db->query(
            $this->queryBuilder
                ->select()
                ->from('type_room')
                ->orderBy('id', 'ASC')
                ->sql()
        );

        return $query;
    }

    public function getRoom($id)
    {
        $region = new Room($id);

        return $region->findOne();
    }
}