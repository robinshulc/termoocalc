<?php

namespace Cms\Model\Material;

use Engine\Model;

class MaterialRepository extends Model
{
    /**
     * @param array $params
     * @return int
     */
    public function add($params = [])
    {
        if (empty($params)) {
            return 0;
        }

        $menu = new Material;
        $menu->setName($params['name']);
        $menuId = $menu->save();

        return $menuId;
    }

    public function getList()
    {
        $query = $this->db->query(
            $this->queryBuilder
                ->select()
                ->from('material')
                ->orderBy('id', 'ASC')
                ->sql()
        );

        return $query;
    }

    public function getMaterial($id)
    {
        $region = new Material($id);

        return $region->findOne();
    }


    public function getMaterialCategory()
    {

        $sql = $this->queryBuilder->select()
            ->from('material_category')
            ->where('parent', 0)
            ->orderBy('id', 'ASC')
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }


    public function getMaterialCategoryByParent($id)
    {
        if (empty($id)) {
            return 0;
        }

        $sql = $this->queryBuilder->select()
            ->from('material_category')
            ->where('parent', $id)
            ->orderBy('id', 'ASC')
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }


    public function getMaterialByParent($id)
    {
        if (empty($id)) {
            return 0;
        }

        $sql = $this->queryBuilder->select()
            ->from('material')
            ->where('category', $id)
            ->orderBy('id', 'ASC')
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }


    public function getMaterialsByParams($params)
    {
        if (empty($params)) {
            return 0;
        }

        $sql = $this->queryBuilder->select()
            ->from('material_new')
            ->where($params['col'], $params['val'], $params['operator'])
            ->orderBy('id', 'ASC')
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }

}