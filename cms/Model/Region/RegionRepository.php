<?php

namespace Cms\Model\Region;

use Engine\Model;

class RegionRepository extends Model
{


    /**
     * @param array $params
     * @return int
     */
    public function add($params = [])
    {
        if (empty($params)) {
            return 0;
        }

        $menu = new Region;
        $menu->setName($params['name']);
        $menuId = $menu->save();

        return $menuId;
    }

    public function getList()
    {
        $query = $this->db->query(
            $this->queryBuilder
                ->select()
                ->from('region')
                ->orderBy('id', 'ASC')
                ->sql()
        );

        return $query;
    }


    public function getRegion($id)
    {
        $region = new Region($id);

        return $region->findOne();
    }
}