<?php

namespace Cms\Model\City;

use Engine\Model;

class CityRepository extends Model
{
    /**
     * @param array $params
     * @return int
     */
    public function add($params = [])
    {
        if (empty($params)) {
            return 0;
        }

        $menu = new City;
        $menu->setName($params['name']);
        $menuId = $menu->save();

        return $menuId;
    }

    public function getList()
    {
        $query = $this->db->query(
            $this->queryBuilder
                ->select()
                ->from('city')
                ->orderBy('id', 'ASC')
                ->sql()
        );

        return $query;
    }

    public function getCitiesByReg($id)
    {
        if (empty($id)) {
            return 0;
        }

        $sql = $this->queryBuilder->select('id, name')
            ->from('city')
            ->where('region', $id)
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }


    public function getCity($id)
    {
        $region = new City($id);

        return $region->findOne();
    }
}