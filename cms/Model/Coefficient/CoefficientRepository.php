<?php

namespace Cms\Model\Coefficient;

use Engine\Model;

class CoefficientRepository extends Model
{
    /**
     * @param array $params
     * @return int
     */
    public function add($params = [])
    {
        if (empty($params)) {
            return 0;
        }

        $menu = new Coefficient;
        $menu->setName($params['name']);
        $menuId = $menu->save();

        return $menuId;
    }

    public function getList()
    {
        $query = $this->db->query(
            $this->queryBuilder
                ->select()
                ->from('coefficient')
                ->orderBy('id', 'ASC')
                ->sql()
        );

        return $query;
    }


    public function getCoefficient($id)
    {
        $region = new Coefficient($id);

        return $region->findOne();
    }


    public function getDewData($params)
    {

        if (empty($params)) {
            return 0;
        }


        $sql = $this->queryBuilder->select()
            ->from('dew_point')
            ->where('temperature', $params['val'])
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query[0] : null;

    }


}