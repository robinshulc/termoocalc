<?php

namespace Cms\Controller;
use Data;

class DataController extends CmsController
{
    public function getCities()
    {
        $cities='<option value="">Выберите город </option>';
        $params = $this->request->post;
        $result = Data::getCitiesByReg($params['region']);
        foreach ($result as $item){
            if($item != null){
                $cities.='<option value="'.$item->id.'">'.$item->name.'</option>';
            }
        }
        print_r($cities);
    }

    public function getCityParams()
    {
        $city='';
        $params = $this->request->post;

        $result = Data::getCity($params['city']);
        if(!isset($result)){
            die();
        }
        foreach ($result as $key=>$value){
//            if($item != null){
                $city.='<tr>
                            <th scope="row">
                                '.$key.'
                            </th>
                            <td>
                                '.$value.'
                            </td>
                        </tr>';
//            }
        }
        print_r($city);
    }

    public function getBuildingParams()
    {
        $building='';
        $params = $this->request->post;
        $result = Data::getBuildingData($params['building']);
        foreach ($result[0] as $key=>$value){

            $building.='<tr>
                            <th scope="row">
                                '.$key.'
                            </th>
                            <td>
                                '.$value.'
                            </td>';

        foreach ($result[1] as $key1=>$value1){
            if($key == $key1){
            $building.='    <td>
                                '.$value1.'
                            </td>
                        </tr>';
                }
            }
        }
        print_r($building);
    }

    public function getRoomParams()
    {
        $room='';
        $params = $this->request->post;
        $result = Data::getRoom($params['room']);
        foreach ($result as $key=>$value){
//            if($item != null){
            $room.='<tr>
                            <th scope="row">
                                '.$key.'
                            </th>
                            <td>
                                '.$value.'
                            </td>
                        </tr>';
//            }
        }
        print_r($room);
    }
    public function getRoomType()
    {
        $room='';
        $params = $this->request->post;
        $result = Data::getRoom($params['room']);
        $room = json_encode(get_object_vars($result));
        print_r($room);
    }

    public function getCitiesBuildingsParams()
    {
        $city_building='';
        $params = $this->request->post;

        $city = Data::getCity($params['city']);

        $building = Data::getBuilding($params['building']);
        $building_data = Data::getBuildingData($params['building']);

        $rooms = Data::getRooms();

        $td01 = '';
        $td02 = '';
        $td03 = '';
        $td04 = '';
        $td05 = '';
        $td06 = '';
        $td07 = '';
        $td08 = '';
        $td09 = '';
        $td10 = '';
        $td11 = '';
        $td12 = '';
        $td13 = '';
        $td14 = '';
        $td15 = '';
        $td16 = '';
        $td17 = '';
        $td18 = '';
        $td19 = '';
        $td20 = '';
        $td21 = '';
        $td22 = '';
        $td23 = '';
        $td24 = '';
        $td25 = '';
        $td26 = '';
        $td27 = '';
        $td28 = '';
        $td29 = '';
        $td30 = '';
        $td31 = '';
        $td32 = '';
        $td33 = '';
        $td34 = '';
        $td35 = '';
        $td36 = '';
        $td37 = '';
        $td38 = '';
        $td39 = '';
        $td40 = '';
        $td41 = '';
        $td42 = '';
        $td43 = '';
        $td44 = '';
        $td45 = '';
        $td46 = '';
        $td47 = '';
        $td48 = '';
        $td49 = '';
        $td50 = '';
        $td51 = '';
        $td52 = '';
        $td53 = '';
        $td54 = '';
        $td55 = '';
        $td56 = '';
        $td57 = '';
        $td58 = '';
        $td59 = '';
        $td60 = '';
        $td61 = '';


        if(isset($rooms)){
            $td06 = $rooms[0]->stp15;
            $td07 = $rooms[0]->stp16;
            $td10 = $rooms[1]->stp15;
            $td11 = $rooms[1]->stp16;
            $td14 = $rooms[2]->stp15;
            $td15 = $rooms[2]->stp16;
            $td18 = $rooms[3]->stp15;
            $td19 = $rooms[3]->stp16;
            $td22 = $rooms[4]->stp15;
            $td23 = $rooms[4]->stp16;
            $td26 = $rooms[5]->stp15;
            $td27 = $rooms[5]->stp16;
            $td30 = $rooms[6]->stp15;
            $td31 = $rooms[6]->stp16;
            $td34 = $rooms[7]->stp15;
            $td35 = $rooms[7]->stp16;
        }

        if(isset($city)){
            $td01 = $city->name;
            $td03 = $city->st024;
            $td04 = $city->st004;
            if (
                isset($city->st008) &&
                isset($building->id) &&
                isset($rooms[0]->stp02)
            ) {
                $td05 = $rooms[0]->stp02;
                if ($city->st008 <= -31) {
                    $rooms[0]->stp02 = 21;
                    $rooms[0]->stp03 = 23;
                    $rooms[0]->stp04 = 20;
                    $rooms[0]->stp06 = 20;
                    $rooms[0]->stp07 = 22;
                    $rooms[0]->stp08 = 19;
                } else {
                    $rooms[0]->stp02 = 20;
                    $rooms[0]->stp03 = 22;
                    $rooms[0]->stp04 = 18;
                    $rooms[0]->stp06 = 19;
                    $rooms[0]->stp07 = 20;
                    $rooms[0]->stp08 = 17;
                }
                if($building != null){
//                    echo "<pre>"''
//                    print_r($rooms);exit;
                    if ($building->id < 3) {
                        if ($rooms[0]->stp02 < 20) {
                            $td05 = 20;
                        } elseif ($rooms[0]->stp02 > 22) {
                            $td05 = 22;
                        } else {
                            $td05 = $rooms[0]->stp02;
                        }
                    }
                    if($building->id  == 3){
                        if($rooms[0]->stp02 < 16){
                            $td05 = 16;
                        }elseif ($rooms[0]->stp02 > 21){
                            $td05 = 21;
                        }else{
                            $td05 = $rooms[0]->stp02;
                        }
                    }
                }

            }

            if (isset($city->st004) && $td07!='') {
                $room_data = $td07;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td08 = 'b';
                    } else {
                        $td08 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td08 = 'a';
                    } else {
                        $td08 = 'b';

                    }
                } else {
                    $td08 = 'b';
                }
            }


            if (
                isset($building->id) &&
                isset($rooms[1]->stp02)
            ) {
                $td09 = $rooms[1]->stp02;

                if($building != null){
                    if ($building->id < 3) {

                        if ($rooms[1]->stp02 < 20) {
                            $td09 = 20;
                        } elseif ($rooms[1]->stp02 > 22) {
                            $td09 = 22;
                        } else {
                            $td09 = $rooms[1]->stp02;
                        }
                    }
                    if($building->id  == 3){
                        if($rooms[1]->stp02 < 16){
                            $td09 = 16;
                        }elseif ($rooms[1]->stp02 > 21){
                            $td09 = 21;
                        }else{
                            $td09 = $rooms[1]->stp02;
                        }
                    }
                }


            }


            if (isset($city->st004) && $td11!='') {
                $room_data = $td11;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td12 = 'b';
                    } else {
                        $td12 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td12 = 'a';
                    } else {
                        $td12 = 'b';

                    }
                } else {
                    $td12 = 'b';
                }
            }


            if (
                isset($building->id) &&
                isset($rooms[2]->stp02)
            ) {
                $td13 = $rooms[2]->stp02;

                if($building != null){
                    if ($building->id < 3) {
                        if ($rooms[2]->stp02 < 20) {
                            $td13 = 20;
                        } elseif ($rooms[2]->stp02 > 22) {
                            $td13 = 22;
                        } else {
                            $td13 = $rooms[2]->stp02;
                        }
                    }
                    if($building->id  == 3){
                        if($rooms[2]->stp02 < 16){
                            $td13 = 16;
                        }elseif ($rooms[2]->stp02 > 21){
                            $td13 = 21;
                        }else{
                            $td13 = $rooms[2]->stp02;
                        }
                    }
                }


            }




            if (isset($city->st004) && $td15!='') {
                $room_data = $td15;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td16 = 'b';
                    } else {
                        $td16 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td16 = 'a';
                    } else {
                        $td16 = 'b';

                    }
                } else {
                    $td16 = 'b';
                }
            }

            if (
                isset($building->id) &&
                isset($rooms[3]->stp02)
            ) {
                $td17 = $rooms[3]->stp02;

                if($building != null){
                    if ($building->id < 3) {
                        if ($rooms[3]->stp02 < 20) {
                            $td17 = 20;
                        } elseif ($rooms[3]->stp02 > 22) {
                            $td17 = 22;
                        } else {
                            $td17 = $rooms[3]->stp02;
                        }
                    }
                    if($building->id  == 3){
                        if($rooms[3]->stp02 < 16){
                            $td17 = 16;
                        }elseif ($rooms[3]->stp02 > 21){
                            $td17 = 21;
                        }else{
                            $td17 = $rooms[3]->stp02;
                        }
                    }
                }


            }




            if (isset($city->st004) && $td19!='') {
                $room_data = $td19;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td20 = 'b';
                    } else {
                        $td20 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td20 = 'a';
                    } else {
                        $td20 = 'b';

                    }
                } else {
                    $td20 = 'b';
                }
            }

            if (
                isset($building->id) &&
                isset($rooms[4]->stp02)
            ) {
                $td21 = $rooms[4]->stp02;

                if($building != null){
                    if ($building->id < 3) {
                        if ($rooms[4]->stp02 < 20) {
                            $td21 = 20;
                        } elseif ($rooms[4]->stp02 > 22) {
                            $td21 = 22;
                        } else {
                            $td21 = $rooms[4]->stp02;
                        }
                    }
                    if($building->id  == 3){
                        if($rooms[4]->stp02 < 16){
                            $td21 = 16;
                        }elseif ($rooms[4]->stp02 > 21){
                            $td21 = 21;
                        }else{
                            $td21 = $rooms[4]->stp02;
                        }
                    }
                }


            }


            if (isset($city->st004) && $td23!='') {
                $room_data = $td23;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td24 = 'b';
                    } else {
                        $td24 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td24 = 'a';
                    } else {
                        $td24 = 'b';

                    }
                } else {
                    $td24 = 'b';
                }
            }

            if (
                isset($building->id) &&
                isset($rooms[5]->stp02)
            ) {
                $td25 = $rooms[5]->stp02;

                if($building != null){
                    if ($building->id < 3) {
                        if ($rooms[5]->stp02 < 20) {
                            $td25 = 20;
                        } elseif ($rooms[5]->stp02 > 22) {
                            $td25 = 22;
                        } else {
                            $td25 = $rooms[5]->stp02;
                        }
                    }
                    if($building->id  == 3){
                        if($rooms[5]->stp02 < 16){
                            $td25 = 16;
                        }elseif ($rooms[5]->stp02 > 21){
                            $td25 = 21;
                        }else{
                            $td25 = $rooms[5]->stp02;
                        }
                    }
                }


            }

            if (isset($city->st004) && $td27!='') {
                $room_data = $td27;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td28 = 'b';
                    } else {
                        $td28 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td28 = 'a';
                    } else {
                        $td28 = 'b';

                    }
                } else {
                    $td28 = 'b';
                }
            }

            if (
                isset($building->id) &&
                isset($rooms[6]->stp02)
            ) {
                $td29 = $rooms[6]->stp02;

                if($building != null){
                    if ($building->id < 3) {
                        if ($rooms[6]->stp02 < 20) {
                            $td29 = 20;
                        } elseif ($rooms[6]->stp02 > 22) {
                            $td29 = 22;
                        } else {
                            $td29 = $rooms[6]->stp02;
                        }
                    }
                    if($building->id  == 3){
                        if($rooms[6]->stp02 < 16){
                            $td29 = 16;
                        }elseif ($rooms[6]->stp02 > 21){
                            $td29 = 21;
                        }else{
                            $td29 = $rooms[6]->stp02;
                        }
                    }
                }


            }


            if (isset($city->st004) && $td31!='') {
                $room_data = $td31;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td32 = 'b';
                    } else {
                        $td32 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td32 = 'a';
                    } else {
                        $td32 = 'b';

                    }
                } else {
                    $td32 = 'b';
                }
            }

            if (
                isset($building->id) &&
                isset($rooms[7]->stp02)
            ) {
                $td33 = $rooms[7]->stp02;

                if($building != null){
                    if ($building->id < 3) {
                        if ($rooms[7]->stp02 < 20) {
                            $td33 = 20;
                        } elseif ($rooms[7]->stp02 > 22) {
                            $td33 = 22;
                        } else {
                            $td33 = $rooms[7]->stp02;
                        }
                    }
                    if($building->id  == 3){
                        if($rooms[7]->stp02 < 16){
                            $td33 = 16;
                        }elseif ($rooms[7]->stp02 > 21){
                            $td33 = 21;
                        }else{
                            $td33 = $rooms[7]->stp02;
                        }
                    }
                }


            }


            if (isset($city->st004) && $td35!='') {
                $room_data = $td35;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td36 = 'b';
                    } else {
                        $td36 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td36 = 'a';
                    } else {
                        $td36 = 'b';

                    }
                } else {
                    $td36 = 'b';
                }
            }


            $td37 = $city->st008;

            if (
                isset($building->id) &&
                isset($city->st015) &&
                isset($city->st017)
            ) {

                if ($building != null) {
                    if ($building->id == 2) {
                        $td38 = $city->st017;
                    } else {
                        $td38 = $city->st015;
                    }
                }
            }


            if (
                isset($building->id) &&
                isset($city->st014) &&
                isset($city->st016)
            ) {

                if ($building != null) {
                    if ($building->id == 2) {
                        $td39 = $city->st016;
                    } else {
                        $td39 = $city->st014;
                    }
                }
            }


            if (isset($city->st013)) {
                $td40 = $city->st013;
            }


            if (isset($city->st012)) {
                $td41 = $city->st012;
            }

            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050) &&
                isset($city->st051) &&
                isset($city->st052) &&
                isset($city->st053) &&
                isset($city->st054) &&
                isset($city->st055) &&
                isset($city->st056) &&
                isset($city->st057) &&
                isset($city->st058) &&
                isset($city->st059) &&
                isset($city->st060) &&
                isset($city->st061) &&
                isset($city->st062) &&
                isset($city->st063)
            ) {

                $months = 0;
                $sumPressure = 0;
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );
                $pressure = array(
                    $city->st052,
                    $city->st053,
                    $city->st054,
                    $city->st055,
                    $city->st056,
                    $city->st057,
                    $city->st058,
                    $city->st059,
                    $city->st060,
                    $city->st061,
                    $city->st062,
                    $city->st063,
                );

                foreach($temperatureByMonth as $key => $value) {
                    if ($value < 0) {
                        $sumPressure += $pressure[$key];
                        $months++;
                    }
                }
                if ($months == 0 || $sumPressure == 0){
                    $td42 = '0';
                }else{
                    $td42 = round($sumPressure / $months, 6);
                }

            }



            if (
                isset($city->st052) &&
                isset($city->st053) &&
                isset($city->st054) &&
                isset($city->st055) &&
                isset($city->st056) &&
                isset($city->st057) &&
                isset($city->st058) &&
                isset($city->st059) &&
                isset($city->st060) &&
                isset($city->st061) &&
                isset($city->st062) &&
                isset($city->st063)
            ) {

                $months = 0;
                $sumPressure = 0;
                $pressure = array(
                    $city->st052,
                    $city->st053,
                    $city->st054,
                    $city->st055,
                    $city->st056,
                    $city->st057,
                    $city->st058,
                    $city->st059,
                    $city->st060,
                    $city->st061,
                    $city->st062,
                    $city->st063,
                );


                foreach($pressure as $key => $value) {
                    $sumPressure += $value;
                    $months++;
                }
                if ($months == 0 || $sumPressure == 0){
                    $td43 = '0';
                }else{
                    $td43 = round($sumPressure / $months, 6);
                }

            }


            if (
                $td05 != '' &&
                $td38 != '' &&
                $td39 != ''
            ){
                $td44 = round((
                        $td05 -
                        $td38
                    ) * $td39, 6);
            }

            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050)
            ) {


                $sumNegativeTemperatures = 0;
                $months = 0;
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );

                foreach($temperatureByMonth as $key => $value) {
                    if ($value < -5) {
                        $sumNegativeTemperatures += $value;
                        $months++;
                    }
                }

                if ($months == 0 || $sumNegativeTemperatures == 0){
                    $td45 = '0';
                }else{
                    $td45 = round($sumNegativeTemperatures / $months, 6);
                }

            }

            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050)
            ) {

                $day = 0;
                $dayInMount = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );


                foreach($temperatureByMonth as $key => $value) {
                    if ($value < -5) $day += $dayInMount[$key];
                }

                if ($day == 0){
                    $td46 = '0';
                }else{
                    $td46 = round($day/30, 0);
                }

            }


            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050) &&
                isset($city->st051) &&
                isset($city->st052) &&
                isset($city->st053) &&
                isset($city->st054) &&
                isset($city->st055) &&
                isset($city->st056) &&
                isset($city->st057) &&
                isset($city->st058) &&
                isset($city->st059) &&
                isset($city->st060) &&
                isset($city->st061) &&
                isset($city->st062) &&
                isset($city->st063)
            ) {

                $months = 0;
                $sumPressure = 0;
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );
                $pressure = array(
                    $city->st052,
                    $city->st053,
                    $city->st054,
                    $city->st055,
                    $city->st056,
                    $city->st057,
                    $city->st058,
                    $city->st059,
                    $city->st060,
                    $city->st061,
                    $city->st062,
                    $city->st063,
                );

                foreach($temperatureByMonth as $key => $value) {
                    if ($value < -5) {
                        $sumPressure += $pressure[$key];
                        $months++;
                    }
                }
                if ($months == 0 || $sumPressure == 0){
                    $td47 = '0';
                }else{
                    $td47 = round($sumPressure / $months, 6);
                }

            }



            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050)
            ) {



                $sumNegativeTemperatures = 0;
                $months = 0;
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );
                foreach($temperatureByMonth as $key => $value) {
                    if ($value >= -5 && $value <= 5) {
                        $sumNegativeTemperatures += $value;
                        $months++;
                    }
                }

                if ($months == 0 || $sumNegativeTemperatures == 0){
                    $td48 = '0';
                }else{
                    $td48 = round($sumNegativeTemperatures / $months, 6);
                }

            }



            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050)
            ) {

                $day = 0;
                $dayInMount = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );


                foreach($temperatureByMonth as $key => $value) {
                    if ($value >= -5 && $value <= 5) $day += $dayInMount[$key];
                }
                if ($day == 0){
                    $td49 = '0';
                }else{
                    $td49 = round($day/30, 0);

                }

            }



            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050) &&
                isset($city->st051) &&
                isset($city->st052) &&
                isset($city->st053) &&
                isset($city->st054) &&
                isset($city->st055) &&
                isset($city->st056) &&
                isset($city->st057) &&
                isset($city->st058) &&
                isset($city->st059) &&
                isset($city->st060) &&
                isset($city->st061) &&
                isset($city->st062) &&
                isset($city->st063)
            ) {

                $months = 0;
                $sumPressure = 0;
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );
                $pressure = array(
                    $city->st052,
                    $city->st053,
                    $city->st054,
                    $city->st055,
                    $city->st056,
                    $city->st057,
                    $city->st058,
                    $city->st059,
                    $city->st060,
                    $city->st061,
                    $city->st062,
                    $city->st063,
                );
                foreach($temperatureByMonth as $key => $value) {
                    if ($value >= -5 && $value <= 5) {
                        $sumPressure += $pressure[$key];
                        $months++;
                    }
                }
                if ($months == 0 || $sumPressure == 0){
                    $td50 = '0';
                }else{
                    $td50 = round($sumPressure / $months, 6);
                }

            }





            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050)
            ) {

                $sumNegativeTemperatures = 0;
                $months = 0;
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );

                foreach($temperatureByMonth as $key => $value) {
                    if ($value > 5) {
                        $sumNegativeTemperatures += $value;
                        $months++;
                    }
                }


                if ($months == 0 || $sumNegativeTemperatures == 0){
                    $td51 = '0';
                }else{
                    $td51 = round($sumNegativeTemperatures / $months, 6);
                }

            }



            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050)
            ) {

                $day = 0;
                $dayInMount = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );


                foreach($temperatureByMonth as $key => $value) {
                    if ($value > 5) $day += $dayInMount[$key];
                }
                if ($day == 0){
                    $td52 = '0';
                }else{
                    $td52 =  round($day/30, 0);
                }

            }




            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050) &&
                isset($city->st051) &&
                isset($city->st052) &&
                isset($city->st053) &&
                isset($city->st054) &&
                isset($city->st055) &&
                isset($city->st056) &&
                isset($city->st057) &&
                isset($city->st058) &&
                isset($city->st059) &&
                isset($city->st060) &&
                isset($city->st061) &&
                isset($city->st062) &&
                isset($city->st063)
            ) {

                $months = 0;
                $sumPressure = 0;
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );
                $pressure = array(
                    $city->st052,
                    $city->st053,
                    $city->st054,
                    $city->st055,
                    $city->st056,
                    $city->st057,
                    $city->st058,
                    $city->st059,
                    $city->st060,
                    $city->st061,
                    $city->st062,
                    $city->st063,
                );
                foreach($temperatureByMonth as $key => $value) {
                    if ($value > 5) {
                        $sumPressure += $pressure[$key];
                        $months++;
                    }
                }
                if ($months == 0 || $sumPressure == 0){
                    $td53 = '0';
                }else{
                    $td53 = round($sumPressure / $months, 6);
                }

            }



            function td54 () {
                global $td;
                if (
                    !isset($_SESSION['data']['typeBuilding']['data']['a']['stg03']) ||
                    !isset($_SESSION['data']['typeBuilding']['data']['b']['stg03']) ||
                    empty($td['td03']) ||
                    empty($td['td44']) ||
                    empty($td['td59'])
                ) {
                    $td['error'] .= 'Ошибка в получении данных td54;' . '<br/><br/>';
                    return '';
                }
                $td['error'] .= 'td54 = ([data][typeBuilding][data][a][stg03] * [td44] + [data][typeBuilding][data][a][stg03]) * [td59] * [td03];' . '<br/><br/>';

                return round((
                        $_SESSION['data']['typeBuilding']['data']['a']['stg03'] *
                        $td['td44'] +
                        $_SESSION['data']['typeBuilding']['data']['b']['stg03']
                    ) * $td['td59'] * $td['td03'], 6);
            }



            $td59 = '1';
            $td60 = '1';
            $td61 = '1';

            if(isset($building_data)){
                if (
                    isset($building_data[0]->stg03) &&
                    isset($building_data[1]->stg03) &&
                    ($td03 != '') &&
                    ($td44 != '') &&
                    ($td59 != '')
                ) {


                    $td54 = round((
                            $building_data[0]->stg03 *
                            $td44 +
                            $building_data[1]->stg03
                        ) * $td59 * $td03, 6);
                }
            }


            if(isset($building_data)){
                if (
                    isset($building_data[0]->stg04) &&
                    isset($building_data[1]->stg04) &&
                    ($td03 != '') &&
                    ($td44 != '') &&
                    ($td61 != '')
                ) {


                    $td55 = round((
                            $building_data[0]->stg04 *
                            $td44 +
                            $building_data[1]->stg04
                        ) * $td61 * $td03, 6);
                }
            }

            if(isset($building_data)){
                if (
                    isset($building_data[0]->stg05) &&
                    isset($building_data[1]->stg05) &&
                    ($td03 != '') &&
                    ($td44 != '') &&
                    ($td61 != '')
                ) {


                    $td56 = round((
                            $building_data[0]->stg05 *
                            $td44 +
                            $building_data[1]->stg05
                        ) * $td61 * $td03, 6);
                }
            }

            if(isset($building)){
                if (
                    isset($building->id) &&
                    ($td03 != '') &&
                    ($td44 != '') &&
                    ($td60 != '')
                ) {

                        if($building->id < 3) {
                            if($td44 < 6000){
                                $td57_1 = 0.000075;
                            }elseif($td44 >= 6000 && $td44 <= 8000){
                                $td57_1 = 0.00005;
                            }else{
                                $td57_1 = 0.000025;
                            }
                        } elseif($building->id == 3){
                            $td57_1 = 0.00005;
                        }else{
                            $td57_1 = 0.000025;
                        }

                        if($building->id < 3) {
                            if($td44 < 6000){
                                $td57_2 = 0.15;
                            }elseif($td44 >= 6000 && $td44 <= 8000){
                                $td57_2 = 0.3;
                            }else{
                                $td57_2 = 0.5;
                            }
                        } elseif($building->id == 3){
                            $td57_2 = 0.2;
                        }else{
                            $td57_2 = 0.2;
                        }



                    $td57 = round((
                            $td57_1 *
                            $td44 +
                            $td57_2
                        ) * $td60 * $td03, 6);
                }
            }


            if(isset($building_data)){
                if (
                    isset($building_data[0]->stg07) &&
                    isset($building_data[1]->stg07) &&
                    ($td03 != '') &&
                    ($td44 != '') &&
                    ($td61 != '')
                ) {


                    $td58 = round((
                            $building_data[0]->stg07 *
                            $td44 +
                            $building_data[1]->stg07
                        ) * $td61 * $td03, 6);
                }
            }

        }

        if(isset($building) && $building != null){
            $td02 = $building->name;
        }

            $city_building.="
<tr><td>Город (td01)</td><td class='td01'>$td01</td></tr>
<tr><td>Выбранный тип здания (td02)</td><td class='td02'>$td02</td></tr>
<tr><td>Назначаемый региональный коэффициент m р (td03)</td><td class='td03'>$td03</td></tr>
<tr><td>Зона влажности в которой находится населённый пункт (td04)</td><td class='td04'>$td04</td></tr>


<tr class='h2_gray'><td class='roomType_1'>Жилая комната</td><td></td></tr>

<tr><td>Внутренняя температура помещения [t в] °С (td05)</td><td class='td05'>$td05</td></tr>
<tr><td>Максимальная влажность помещения [ϕ в] % (td06)</td><td class='td06'>$td06</td></tr>
<tr><td>Влажностный режим помещения (td07)</td><td class='td07'>$td07</td></tr>
<tr><td>Условия эксплуатации конструкций (td08)</td><td class='td08'>$td08</td></tr>


<tr class='h2_gray'><td class='roomType_2'>Кухня</td><td></td></tr>

<tr><td>Внутренняя температура помещения [t в] °С (td09)</td><td class='td09'>$td09</td></tr>
<tr><td>Максимальная влажность помещения [ϕ в] % (td10)</td><td class='td10'>$td10</td></tr>
<tr><td>Влажностный режим помещения (td11)</td><td class='td11'>$td11</td></tr>
<tr><td>Условия эксплуатации конструкций (td12)</td><td class='td12'>$td12</td></tr>


<tr class='h2_gray'><td class='roomType_3'>Туалет</td><td></td></tr>

<tr><td>Внутренняя температура помещения [t в] °С (td13)</td><td class='td13'>$td13</td></tr>
<tr><td>Максимальная влажность помещения [ϕ в] % (td14)</td><td class='td14'>$td14</td></tr>
<tr><td>Влажностный режим помещения (td15)</td><td class='td15'>$td15</td></tr>
<tr><td>Условия эксплуатации конструкций (td16)</td><td class='td16'>$td16</td></tr>


<tr class='h2_gray'><td class='roomType_4'>Ванная или Совмещённый санузел</td><td></td></tr>

<tr><td>Внутренняя температура помещения [t в] °С (td17)</td><td class='td17'>$td17</td></tr>
<tr><td>Максимальная влажность помещения [ϕ в] % (td18)</td><td class='td18'>$td18</td></tr>
<tr><td>Влажностный режим помещения (td19)</td><td class='td19'>$td19</td></tr>
<tr><td>Условия эксплуатации конструкций (td20)</td><td class='td20'>$td20</td></tr>


<tr class='h2_gray'><td class='roomType_5'>Помещения для отдыха и учебных занятий</td><td></td></tr>

<tr><td>Внутренняя температура помещения [t в] °С (td21)</td><td class='td21'>$td21</td></tr>
<tr><td>Максимальная влажность помещения [ϕ в] % (td22)</td><td class='td22'>$td22</td></tr>
<tr><td>Влажностный режим помещения (td23)</td><td class='td23'>$td23</td></tr>
<tr><td>Условия эксплуатации конструкций (td24)</td><td class='td24'>$td24</td></tr>


<tr class='h2_gray'><td class='roomType_6'>Межквартирный коридор</td><td></td></tr>

<tr><td>Внутренняя температура помещения [t в] °С (td25)</td><td class='td25'>$td25</td></tr>
<tr><td>Максимальная влажность помещения [ϕ в] % (td26)</td><td class='td26'>$td26</td></tr>
<tr><td>Влажностный режим помещения (td27)</td><td class='td27'>$td27</td></tr>
<tr><td>Условия эксплуатации конструкций (td28)</td><td class='td28'>$td28</td></tr>


<tr class='h2_gray'><td class='roomType_7'>Вестибюль, лестничная клетка</td><td></td></tr>

<tr><td>Внутренняя температура помещения [t в] °С (td29)</td><td class='td29'>$td29</td></tr>
<tr><td>Максимальная влажность помещения [ϕ в] % (td30)</td><td class='td30'>$td30</td></tr>
<tr><td>Влажностный режим помещения (td31)</td><td class='td31'>$td31</td></tr>
<tr><td>Условия эксплуатации конструкций (td32)</td><td class='td32'>$td32</td></tr>


<tr class='h2_gray'><td class='roomType_8'>Кладовые</td><td></td></tr>

<tr><td>Внутренняя температура помещения [t в] °С (td33)</td><td class='td33'>$td33</td></tr>
<tr><td>Максимальная влажность помещения [ϕ в] % (td34)</td><td class='td34'>$td34</td></tr>
<tr><td>Влажностный режим помещения (td35)</td><td class='td35'>$td35</td></tr>
<tr><td>Условия эксплуатации конструкций (td36)</td><td class='td36'>$td36</td></tr>


<tr class='h2_gray'><td>Прочие параметры</td><td></td></tr>

<tr><td>Наружная температура (наиболее холодной пятидневки обеспеченностью 0,92) [t н] °С (td37)</td><td class='td37'>$td37</td></tr>
<tr><td>Средняя температура наружного воздуха, °С, отопительного периода [t от] °С (td38)</td><td class='td38'>$td38</td></tr>
<tr><td>Количество суток отопительного периода [z от] суток (td39)</td><td class='td39'>$td39</td></tr>
<tr><td>Средняя температура периода с среднемесячными отрицательными температурами [t н отр] °С (td40)</td><td class='td40'>$td40</td></tr>
<tr><td>Количество суток периода со среднемесячными отрицательными температурами [z o] суток (td41)</td><td class='td41'>$td41</td></tr>
<tr><td>Среднее парциальное давление наружного воздуха за период отрицательных температур [е н отр] Па (td42)</td><td class='td42'>$td42</td></tr>
<tr><td>Среднее парциальное давления наружного воздуха за годовой период [е н] Па (td43)</td><td class='td43'>$td43</td></tr>
<tr><td>ГСОП  (градусо-сутки отопительного периода) [ГСОП] °С * сутки (td44)</td><td class='td44'>$td44</td></tr>
<tr><td>Средняя наружная температура зимнего периода [t зима] °С (td45)</td><td class='td45'>$td45</td></tr>
<tr><td>Количество месяцев зимнего периода [Z зима] суток (td46)</td><td class='td46'>$td46</td></tr>
<tr><td>Среднее парциальное давление зимнего периода [е зима] Па (td47)</td><td class='td47'>$td47</td></tr>
<tr><td>Средняя наружная температура весенне-осеннего периода [t весна-осень] °С (td48)</td><td class='td48'>$td48</td></tr>
<tr><td>Количество месяцев весенне-осеннего периода [Z весна-осень] суток (td49)</td><td class='td49'>$td49</td></tr>
<tr><td>Среднее парциальное давление весенне-осеннего периода [е весна-осень] Па (td50)</td><td class='td50'>$td50</td></tr>
<tr><td>Средняя наружная температура летнего периода [t лето] °С (td51)</td><td class='td51'>$td51</td></tr>
<tr><td>Количество месяцев летнего периода [Z лето] суток (td52)</td><td class='td52'>$td52</td></tr>
<tr><td>Среднее парциальное давление летнего периода [е лето] Па (td53)</td><td class='td53'>$td53</td></tr>

<tr><td>Нормируемое значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм стена] (м2 * °С) / Вт (td54)</td><td class='td54'>$td54</td></tr>
<tr><td>Нормируемое значение приведенного сопротивления теплопередаче конструкций кровли (перекрытий) [R o норм кровля] (м2 * °С) / Вт (td55)</td><td class='td55'>$td55</td></tr>
<tr><td>Нормируемое значение приведенного сопротивления теплопередаче конструкций полов [R o норм пол] (м2 * °С) / Вт (td56)</td><td class='td56'>$td56</td></tr>
<tr><td>Нормируемое значение приведенного сопротивления теплопередаче оконных конструкций [R o норм окна] (м2 * °С) / Вт (td57)</td><td class='td57'>$td57</td></tr>
<tr><td>Нормируемое значение приведенного сопротивления теплопередаче фонарей [R o норм фонари] (м2 * °С) / Вт (td58)</td><td class='td58'>$td58</td></tr>
<tr><td>Понижающий коэффициент при превышении удельного расхода отопления стен (td59)</td><td class='td59'>$td59</td></tr>
<tr><td>Понижающий коэффициент при превышении удельного расхода отопления окон (td60)</td><td class='td60'>$td60</td></tr>
<tr><td>Понижающий коэффициент при превышении удельного расхода отопления прочее (td61)</td><td class='td61'>$td61</td></tr>
";

        print_r($city_building);
    }



    public function getMatCat()
    {
        $materials = '';
        $params = $this->request->post;

        $result = Data::getMaterial($params['material_id']);
        if(!isset($result)){
            die();
        }
        $materials.= '<li><table class="table"><tbody><tr>';
        foreach ($result as $key=>$value){
//            if($value != null){
            $style = '';
            if($key == 'id'){ $style = "style='width: 60px;'";}
            $materials.='
                            <th scope="row" '.$style.'>
                                '.$key.'
                            </th>';
//            }
        }
        $materials.= '</tr><tr>';

        foreach ($result as $key=>$value){
//            if($value != null){
            $materials.='  <td class="'.$key.'">
                                '.$value.'
                            </td>';
//            }
        }
        $materials.= '</tr></tbody></table></li>';

        print_r($materials);
    }


    public function getMatByCat()
    {
        $materials = '';
        $params = $this->request->post;
        $result = Data::getMaterialByParent($params['parent']);
        foreach ($result as $item){
            if($item != null){
                $materials .='<option value="'.$item->id.'">'.$item->name.'</option>';
            }
        }
        print_r($materials);
    }



    public function getCoefficient()
    {
        $coefficient = '';
        $params = $this->request->post;
        $result = Data::getCoefficient($params['coefficient']);
            if($result != null){
                $coefficient .=$result->coefficient;
            }
        print_r($coefficient);
    }




    public function getMatCatBlocks()
    {
        $materials = '';
        $params = $this->request->post;
//        print_r($params);exit;

        $result = Data::getMaterial($params['material_id']);
        if(!isset($result)){
            die();
        }
        $materials.= '<tbody><tr>';
        foreach ($result as $key=>$value){
//            if($value != null){
            $style = '';
            if($key == 'id'){ $style = "style='width: 60px;'";}
            $materials.='<th scope="row" '.$style.'>
                            '.$key.'
                         </th>';
//            }
        }
        $materials.= '</tr><tr>';

        foreach ($result as $key=>$value){
//            if($value != null){
            $materials.='  <td class="'.$key.'">
                                '.$value.'
                            </td>';
//            }
        }
        $materials.= '</tr></tbody>';

        print_r($materials);
    }


    public function getDewData()
    {
        $params = $this->request->post;
        $result =Data::getDewData($params);
        if($result != null){
            print_r($result->per_55);
        }
    }
}