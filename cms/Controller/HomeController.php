<?php

namespace Cms\Controller;

class HomeController extends CmsController
{
    public function index()
    {
        $data = ['name' => 'Artem'];
        $this->view->render('index', $data);
    }

    public function news($id)
    {
        echo $id;
    }

    public function walls()
    {
        $this->view->render('walls');
    }

    public function getLoaderHomogeneous()
    {
        $this->view->render('loader/homogeneous');
    }

    public function getLoaderThermal()
    {
        $this->view->render('loader/thermal');
    }

    public function getLoaderBlocksBlock()
    {
        $this->view->render('loader/blocks/block');
    }



    public function getLoaderBlocksSeam()
    {
        $this->view->render('loader/blocks/seam');
    }

    public function getLoaderFrameFrame()
    {
        $this->view->render('loader/frame/frame');
    }


    public function getLoaderFrameFilling()
    {
        $this->view->render('loader/frame/filling');
    }

    public function getLoaderHolder()
    {
        $this->view->render('loader/holder');
    }



    public function getTableHomogeneousThermal()
    {
        $this->view->render('loader/table/homogeneous_thermal');
    }

    public function getTableBlockFilling()
    {
        $this->view->render('loader/table/block_filling');
    }

}