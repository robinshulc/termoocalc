<?php

return [
    'host'     => 'localhost',
    'db_name'  => 'thermocalc_ru',
    'username' => 'root',
    'password' => 'password',
    'charset'  => 'utf8'
];