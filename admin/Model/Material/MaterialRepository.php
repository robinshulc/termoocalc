<?php

namespace Admin\Model\Material;

use Engine\Model;
use Engine\Core\Database\QueryBuilder;

class MaterialRepository extends Model
{
    /**
     * @return mixed
     */
    public function getMaterials()
    {

        $sql = $this->queryBuilder->select()
            ->from('material_new')
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql);
    }

    /**
     * @param $id
     * @return null|\stdClass
     */
    public function getMaterialData($id)
    {
        $Material = new Material($id);

        return $Material->findOne();
    }

    /**
     * @param $params
     * @return mixed
     */
    public function createMaterial($params)
    {

        $Material = new Material;
        $Material->setCategory($params['category']);
        $Material->setCOL3($params['COL3']);
        $Material->setCOL4($params['COL4']);
        $Material->setName($params['name']);
        $Material->setCOL6($params['COL6']);
        $Material->setCOL7($params['COL7']);
        $Material->setCOL8($params['COL8']);
        $Material->setCOL9($params['COL9']);
        $Material->setCOL10($params['COL10']);
        $Material->setCOL11($params['COL11']);
        $Material->setCOL12($params['COL12']);
        $Material->setCOL13($params['COL13']);
        $Material->setCOL14($params['COL14']);
        $Material->setCOL15($params['COL15']);
        $Material->setCOL16($params['COL16']);
        $Material->setCOL17($params['COL17']);
        $Material->setCOL18($params['COL18']);
        $Material->setCOL19($params['COL19']);
        $Material->setCOL20($params['COL20']);
        $Material->setCOL21($params['COL21']);
        $Material->setCOL22($params['COL22']);
        $Material->setCOL23($params['COL23']);
        $Material->setCOL24($params['COL24']);
        $Material->setCOL25($params['COL25']);
        $Material->setCOL26($params['COL26']);
        $Material->setCOL27($params['COL27']);
        $Material->setCOL28($params['COL28']);
        $Material->setCOL29($params['COL29']);
        $Material->setCOL30($params['COL30']);
        $Material->setCOL31($params['COL31']);
        $Material->setCOL32($params['COL32']);
        $Material->setCOL33($params['COL33']);
        $Material->setCOL34($params['COL34']);
        $Material->setCOL35($params['COL35']);
        $Material->setCOL36($params['COL36']);
        $Material->setCOL37($params['COL37']);
        $MaterialId = $Material->save();

        return $MaterialId;
    }

    /**
     * @param $params
     */
    public function updateMaterial($params)
    {

        if (isset($params['material_id'])) {

            $id = $params['material_id'];
            $queryBuilder = new QueryBuilder();
            $sql = $queryBuilder
                ->update('material_new')
                ->set([
                    'category' => $params['category'],
                    'COL3' => $params['COL3'],
                    'COL4' => $params['COL4'],
                    'name' => $params['name'],
                    'COL6' => $params['COL6'],
                    'COL7' => $params['COL7'],
                    'COL8' => $params['COL8'],
                    'COL9' => $params['COL9'],
                    'COL10' => $params['COL10'],
                    'COL11' => $params['COL11'],
                    'COL12' => $params['COL12'],
                    'COL13' => $params['COL13'],
                    'COL14' => $params['COL14'],
                    'COL15' => $params['COL15'],
                    'COL16' => $params['COL16'],
                    'COL17' => $params['COL17'],
                    'COL18' => $params['COL18'],
                    'COL19' => $params['COL19'],
                    'COL20' => $params['COL20'],
                    'COL21' => $params['COL21'],
                    'COL22' => $params['COL22'],
                    'COL23' => $params['COL23'],
                    'COL24' => $params['COL24'],
                    'COL25' => $params['COL25'],
                    'COL26' => $params['COL26'],
                    'COL27' => $params['COL27'],
                    'COL28' => $params['COL28'],
                    'COL29' => $params['COL29'],
                    'COL30' => $params['COL30'],
                    'COL31' => $params['COL31'],
                    'COL32' => $params['COL32'],
                    'COL33' => $params['COL33'],
                    'COL34' => $params['COL34'],
                    'COL35' => $params['COL35'],
                    'COL36' => $params['COL36'],
                    'COL37' => $params['COL37'],

                ])
                ->where('id', $id)->sql();

            $query = $this->db->execute($sql, $queryBuilder->values);
            echo "<pre>";
            print_r($sql);
            echo "</pre>";
            return $query;

        }
    }
}