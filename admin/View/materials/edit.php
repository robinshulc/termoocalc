<?php $this->theme->header(); ?>

    <main>
        <div class="ui container">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <div class="col page-title">
                        <h2 class="ui header">
                            <?= $material->title ?>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="ui grid">
                <div class="twelve wide column">
                    <form id="formPage" class="ui form" action="/admin/material/update/" method="post">
                        <input type="hidden" name="material_id" id="material_id" value="<?= $material->id ?>"/>
                        <div class="field">
                            <label>Category</label>
                            <input type="text" name="category" class="form-control" id="category" value="<?= $material->category;?>">
                        </div>
                        <div class="field">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" id="name" value="<?= $material->name;?>">
                        </div>
                        <div class="field">
                            <label>COL3</label>
                            <input type="text" name="COL3" class="form-control" id="COL3" value="<?= $material->COL3;?>">
                        </div>
                        <div class="field">
                            <label>COL4</label>
                            <input type="text" name="COL4" class="form-control" id="COL4" value="<?= $material->COL4;?>">
                        </div>
                        <div class="field">
                            <label>COL6</label>
                            <input type="text" name="COL6" class="form-control" id="COL6" value="<?= $material->COL6;?>">
                        </div>
                        <div class="field">
                            <label>COL7</label>
                            <input type="text" name="COL7" class="form-control" id="COL7" value="<?= $material->COL7;?>">
                        </div>
                        <div class="field">
                            <label>COL8</label>
                            <input type="text" name="COL8" class="form-control" id="COL8" value="<?= $material->COL8;?>">
                        </div>
                        <div class="field">
                            <label>COL9</label>
                            <input type="text" name="COL9" class="form-control" id="COL9" value="<?= $material->COL9;?>">
                        </div>
                        <div class="field">
                            <label>COL10</label>
                            <input type="text" name="COL10" class="form-control" id="COL10" value="<?= $material->COL10;?>">
                        </div>
                        <div class="field">
                            <label>COL11</label>
                            <input type="text" name="COL11" class="form-control" id="COL11" value="<?= $material->COL11;?>">
                        </div>
                        <div class="field">
                            <label>COL12</label>
                            <input type="text" name="COL12" class="form-control" id="COL12" value="<?= $material->COL12;?>">
                        </div>
                        <div class="field">
                            <label>COL13</label>
                            <input type="text" name="COL13" class="form-control" id="COL13" value="<?= $material->COL13;?>">
                        </div>
                        <div class="field">
                            <label>COL14</label>
                            <input type="text" name="COL14" class="form-control" id="COL14" value="<?= $material->COL14;?>">
                        </div>
                        <div class="field">
                            <label>COL15</label>
                            <input type="text" name="COL15" class="form-control" id="COL15" value="<?= $material->COL15;?>">
                        </div>
                        <div class="field">
                            <label>COL16</label>
                            <input type="text" name="COL16" class="form-control" id="COL16" value="<?= $material->COL16;?>">
                        </div>
                        <div class="field">
                            <label>COL17</label>
                            <input type="text" name="COL17" class="form-control" id="COL17" value="<?= $material->COL17;?>">
                        </div>
                        <div class="field">
                            <label>COL18</label>
                            <input type="text" name="COL18" class="form-control" id="COL18" value="<?= $material->COL18;?>">
                        </div>
                        <div class="field">
                            <label>COL19</label>
                            <input type="text" name="COL19" class="form-control" id="COL19" value="<?= $material->COL19;?>">
                        </div>
                        <div class="field">
                            <label>COL20</label>
                            <input type="text" name="COL20" class="form-control" id="COL20" value="<?= $material->COL20;?>">
                        </div>
                        <div class="field">
                            <label>COL21</label>
                            <input type="text" name="COL21" class="form-control" id="COL21" value="<?= $material->COL21;?>">
                        </div>
                        <div class="field">
                            <label>COL22</label>
                            <input type="text" name="COL22" class="form-control" id="COL22" value="<?= $material->COL22;?>">
                        </div>
                        <div class="field">
                            <label>COL23</label>
                            <input type="text" name="COL23" class="form-control" id="COL23" value="<?= $material->COL23;?>">
                        </div>
                        <div class="field">
                            <label>COL24</label>
                            <input type="text" name="COL24" class="form-control" id="COL24" value="<?= $material->COL24;?>">
                        </div>
                        <div class="field">
                            <label>COL25</label>
                            <input type="text" name="COL25" class="form-control" id="COL25" value="<?= $material->COL25;?>">
                        </div>
                        <div class="field">
                            <label>COL26</label>
                            <input type="text" name="COL26" class="form-control" id="COL26" value="<?= $material->COL26;?>">
                        </div>
                        <div class="field">
                            <label>COL27</label>
                            <input type="text" name="COL27" class="form-control" id="COL27" value="<?= $material->COL27;?>">
                        </div>
                        <div class="field">
                            <label>COL28</label>
                            <input type="text" name="COL28" class="form-control" id="COL28" value="<?= $material->COL28;?>">
                        </div>
                        <div class="field">
                            <label>COL29</label>
                            <input type="text" name="COL29" class="form-control" id="COL29" value="<?= $material->COL29;?>">
                        </div>
                        <div class="field">
                            <label>COL30</label>
                            <input type="text" name="COL30" class="form-control" id="COL30" value="<?= $material->COL30;?>">
                        </div>
                        <div class="field">
                            <label>COL31</label>
                            <input type="text" name="COL31" class="form-control" id="COL31" value="<?= $material->COL31;?>">
                        </div>
                        <div class="field">
                            <label>COL32</label>
                            <input type="text" name="COL32" class="form-control" id="COL32" value="<?= $material->COL32;?>">
                        </div>
                        <div class="field">
                            <label>COL33</label>
                            <input type="text" name="COL33" class="form-control" id="COL33" value="<?= $material->COL33;?>">
                        </div>
                        <div class="field">
                            <label>COL34</label>
                            <input type="text" name="COL34" class="form-control" id="COL34" value="<?= $material->COL34;?>">
                        </div>
                        <div class="field">
                            <label>COL35</label>
                            <input type="text" name="COL35" class="form-control" id="COL35" value="<?= $material->COL35;?>">
                        </div>
                        <div class="field">
                            <label>COL36</label>
                            <input type="text" name="COL36" class="form-control" id="COL36" value="<?= $material->COL36;?>">
                        </div>
                        <div class="field">
                            <label>COL37</label>
                            <input type="text" name="COL37" class="form-control" id="COL37" value="<?= $material->COL37;?>">
                        </div>
                        <button type="submit" class="ui primary button"> Update </button>

                    </form>
                </div>
                <div class="four wide column">
                    <div>
                        <p>Update this material</p>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php $this->theme->footer(); ?>