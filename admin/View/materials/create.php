<?php $this->theme->header(); ?>

    <main>
        <div class="ui container">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <div class="col page-title">
                        <h2 class="ui header">
                            Create post
                        </h2>
                    </div>
                </div>
            </div>
            <div class="ui grid">

                <div class="four wide column">
                    <form id="formPage" class="ui form" action="/admin/materials/create/" method="post">
                        <div class="field">
                            <label>Category</label>
                            <input type="text" name="category" class="form-control" id="category" value="">
                        </div>
                        <div class="field">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" id="name" value="">
                        </div>
                        <div class="field">
                            <label>COL3</label>
                            <input type="text" name="COL3" class="form-control" id="COL3" value="">
                        </div>
                        <div class="field">
                            <label>COL4</label>
                            <input type="text" name="COL4" class="form-control" id="COL4" value="">
                        </div>
                        <div class="field">
                            <label>COL6</label>
                            <input type="text" name="COL6" class="form-control" id="COL6" value="">
                        </div>
                        <div class="field">
                            <label>COL7</label>
                            <input type="text" name="COL7" class="form-control" id="COL7" value="">
                        </div>
                        <div class="field">
                            <label>COL8</label>
                            <input type="text" name="COL8" class="form-control" id="COL8" value="">
                        </div>
                        <div class="field">
                            <label>COL9</label>
                            <input type="text" name="COL9" class="form-control" id="COL9" value="">
                        </div>
                        <div class="field">
                            <label>COL10</label>
                            <input type="text" name="COL10" class="form-control" id="COL10" value="">
                        </div>
                        <div class="field">
                            <label>COL11</label>
                            <input type="text" name="COL11" class="form-control" id="COL11" value="">
                        </div>
                        <div class="field">
                            <label>COL12</label>
                            <input type="text" name="COL12" class="form-control" id="COL12" value="">
                        </div>
                        <div class="field">
                            <label>COL13</label>
                            <input type="text" name="COL13" class="form-control" id="COL13" value="">
                        </div>
                        <div class="field">
                            <label>COL14</label>
                            <input type="text" name="COL14" class="form-control" id="COL14" value="">
                        </div>
                        <div class="field">
                            <label>COL15</label>
                            <input type="text" name="COL15" class="form-control" id="COL15" value="">
                        </div>
                        <div class="field">
                            <label>COL16</label>
                            <input type="text" name="COL16" class="form-control" id="COL16" value="">
                        </div>
                        <div class="field">
                            <label>COL17</label>
                            <input type="text" name="COL17" class="form-control" id="COL17" value="">
                        </div>
                        <div class="field">
                            <label>COL18</label>
                            <input type="text" name="COL18" class="form-control" id="COL18" value="">
                        </div>
                        <div class="field">
                            <label>COL19</label>
                            <input type="text" name="COL19" class="form-control" id="COL19" value="">
                        </div>
                        <div class="field">
                            <label>COL20</label>
                            <input type="text" name="COL20" class="form-control" id="COL20" value="">
                        </div>
                        <div class="field">
                            <label>COL21</label>
                            <input type="text" name="COL21" class="form-control" id="COL21" value="">
                        </div>
                        <div class="field">
                            <label>COL22</label>
                            <input type="text" name="COL22" class="form-control" id="COL22" value="">
                        </div>
                        <div class="field">
                            <label>COL23</label>
                            <input type="text" name="COL23" class="form-control" id="COL23" value="">
                        </div>
                        <div class="field">
                            <label>COL24</label>
                            <input type="text" name="COL24" class="form-control" id="COL24" value="">
                        </div>
                        <div class="field">
                            <label>COL25</label>
                            <input type="text" name="COL25" class="form-control" id="COL25" value="">
                        </div>
                        <div class="field">
                            <label>COL26</label>
                            <input type="text" name="COL26" class="form-control" id="COL26" value="">
                        </div>
                        <div class="field">
                            <label>COL27</label>
                            <input type="text" name="COL27" class="form-control" id="COL27" value="">
                        </div>
                        <div class="field">
                            <label>COL28</label>
                            <input type="text" name="COL28" class="form-control" id="COL28" value="">
                        </div>
                        <div class="field">
                            <label>COL29</label>
                            <input type="text" name="COL29" class="form-control" id="COL29" value="">
                        </div>
                        <div class="field">
                            <label>COL30</label>
                            <input type="text" name="COL30" class="form-control" id="COL30" value="">
                        </div>
                        <div class="field">
                            <label>COL31</label>
                            <input type="text" name="COL31" class="form-control" id="COL31" value="">
                        </div>
                        <div class="field">
                            <label>COL32</label>
                            <input type="text" name="COL32" class="form-control" id="COL32" value="">
                        </div>
                        <div class="field">
                            <label>COL33</label>
                            <input type="text" name="COL33" class="form-control" id="COL33" value="">
                        </div>
                        <div class="field">
                            <label>COL34</label>
                            <input type="text" name="COL34" class="form-control" id="COL34" value="">
                        </div>
                        <div class="field">
                            <label>COL35</label>
                            <input type="text" name="COL35" class="form-control" id="COL35" value="">
                        </div>
                        <div class="field">
                            <label>COL36</label>
                            <input type="text" name="COL36" class="form-control" id="COL36" value="">
                        </div>
                        <div class="field">
                            <label>COL37</label>
                            <input type="text" name="COL37" class="form-control" id="COL37" value="">
                        </div>
                        <button type="submit" class="ui primary button"> Update </button>

                    </form>
                </div>
            </div>
        </div>
    </main>

<?php $this->theme->footer(); ?>

