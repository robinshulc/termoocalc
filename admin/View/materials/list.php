<?php $this->theme->header(); ?>

    <main>
        <div class="ui container">
            <div class="row">
                <div class="col page-title">
                    <h2 class="ui header">
                        materials
                        <a href="/admin/materials/create/" class="ui primary button right floated item">
                            Create material
                        </a>
                    </h2>
                </div>
            </div>

            <table class="ui very basic table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Category</th>
<!--                    <th>COL4</th>-->
<!--                    <th>COL6</th>-->
<!--                    <th>COL7</th>-->
<!--                    <th>COL8</th>-->
<!--                    <th>COL9</th>-->
<!--                    <th>COL10</th>-->
<!--                    <th>COL11</th>-->
<!--                    <th>COL12</th>-->
<!--                    <th>COL13</th>-->
<!--                    <th>COL14</th>-->
<!--                    <th>COL15</th>-->
<!--                    <th>COL16</th>-->
<!--                    <th>COL17</th>-->
<!--                    <th>COL18</th>-->
<!--                    <th>COL19</th>-->
<!--                    <th>COL20</th>-->
<!--                    <th>COL21</th>-->
<!--                    <th>COL22</th>-->
<!--                    <th>COL23</th>-->
<!--                    <th>COL24</th>-->
<!--                    <th>COL25</th>-->
<!--                    <th>COL26</th>-->
<!--                    <th>COL27</th>-->
<!--                    <th>COL28</th>-->
<!--                    <th>COL29</th>-->
<!--                    <th>COL30</th>-->
<!--                    <th>COL31</th>-->
<!--                    <th>COL32</th>-->
<!--                    <th>COL33</th>-->
<!--                    <th>COL34</th>-->
<!--                    <th>COL35</th>-->
<!--                    <th>COL36</th>-->
<!--                    <th>COL37</th>-->
                </tr>
                </thead>
                <tbody>
                <?php foreach($materials as $material): ?>
                    <tr>
                        <th scope="row">
                            <?= $material->id ?>
                        </th>
                        <td>
                            <a href="/admin/materials/edit/<?= $material->id ?>">
                                <?= $material->name ?>
                            </a>
                        </td>
                        <td><?=$material->category;?></td>

                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </main>

<?php $this->theme->footer(); ?>