<?php

namespace Admin\Controller;

class MaterialController extends AdminController
{
    public function listing()
    {
        $this->load->model('Material');

        $this->data['materials'] = $this->model->material->getMaterials();

        $this->view->render('materials/list', $this->data);
    }

    public function create()
    {
        $this->view->render('materials/create');
    }

    public function edit($id)
    {
        $this->load->model('Material');

        $this->data['material'] = $this->model->material->getMaterialData($id);

        $this->view->render('materials/edit', $this->data);
    }

    public function add()
    {
        $this->load->model('Material');

        $params = $this->request->post;

        if (isset($params['name'])) {
            $materialId = $this->model->material->createMaterial($params);
            echo $materialId;
        }
    }

    public function update()
    {
        $this->load->model('Material');

        $params = $this->request->post;

        if (isset($params['name'])) {
            $materialId = $this->model->material->updateMaterial($params);
            echo $materialId;
        }
    }
}