<?php

namespace Engine\Core\Template;

use Engine\DI\DI;
use Cms\Model\Region\RegionRepository;
use Cms\Model\Room\RoomRepository;
use Cms\Model\City\CityRepository;
use Cms\Model\Building\BuildingRepository;
use Cms\Model\Material\MaterialRepository;
use Cms\Model\Coefficient\CoefficientRepository;


/**
 * Class Menu
 * @package Engine\Core\Template
 */
class Data
{
    /**
     * @var DI
     */
    protected static $di;

    /**
     * @var $regionRepository
     */
    protected static $regionRepository;

    /**
     * @var $roomRepository
     */
    protected static $roomRepository;

    protected static $cityRepository;

    protected static $buildingRepository;

    protected static $materialRepository;

    protected static $coefficientRepository;

    /**
     * Menu constructor.
     * @param $di
     */
    public function __construct($di)
    {
        self::$di = $di;
        self::$regionRepository = new RegionRepository(self::$di);
        self::$roomRepository = new RoomRepository(self::$di);
        self::$cityRepository = new CityRepository(self::$di);
        self::$buildingRepository = new BuildingRepository(self::$di);
        self::$materialRepository = new MaterialRepository(self::$di);
        self::$coefficientRepository = new CoefficientRepository(self::$di);
    }


//    regions

    /**
     * @return object
     */
    public static function getRegions()
    {
        return self::$regionRepository->getList();
    }

    /**
     * @param int $regionId
     * @return mixed
     */
    public static function getRegion($regionId)
    {
        return self::$regionRepository->getRegion($regionId);
    }
//end regions
//buildings
    public static function getBuildings()
    {
        return self::$buildingRepository->getList();
    }

    public static function getBuildingData($id)
    {
        return self::$buildingRepository->getBuildingData($id);
    }

    public static function getBuilding($id)
    {
        return self::$buildingRepository->getBuilding($id);
    }

//    end buildings

//city
    public static function getCitiesByReg($id)
    {
        return self::$cityRepository->getCitiesByReg($id);
    }

    public static function getCity($id)
    {
        return self::$cityRepository->getCity($id);
    }
//end city

//rooms
    public static function getRooms()
    {
        return self::$roomRepository->getList();
    }


    public static function getRoom($id)
    {
        return self::$roomRepository->getRoom($id);
    }
//end rooms

//materials

    public static function getMaterials()
    {
        return self::$materialRepository->getList();
    }


    public static function getMaterial($id)
    {
        return self::$materialRepository->getMaterial($id);
    }

    public static function getMaterialCategory()
    {
        return self::$materialRepository->getMaterialCategory();
    }

    public static function getMaterialCategoryByParent($id)
    {
        return self::$materialRepository->getMaterialCategoryByParent($id);
    }

    public static function getMaterialByParent($id)
    {
        return self::$materialRepository->getMaterialByParent($id);
    }

    public static function getMaterialsByParams($params)
    {
        return self::$materialRepository->getMaterialsByParams($params);
    }
//end materials

//coefficient

    public static function getCoefficientList()
    {
        return self::$coefficientRepository->getList();
    }

    public static function getCoefficient($id)
    {
        return self::$coefficientRepository->getCoefficient($id);
    }

//end coefficient


    public static function getDewData($params)
    {
        return self::$coefficientRepository->getDewData($params);
    }
}